<?php

use Cake\Core\Configure;

if (file_exists(CONFIG . 'cas.php')) {
    Configure::load('cas');
} else {
    $config = [
        'version' => '3.0',
        'hostname' => '',
        'port' => 443,
        'uri' => '/cas',
        'proxy_mode' => false,
        'proxy_list' => [],
        'debug' => false,
        'debug_file' => LOGS . 'cas.log',
    ];
    Configure::write('Cas', $config);
}
