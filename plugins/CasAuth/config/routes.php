<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'CasAuth',
    ['path' => '/cas-auth'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
