<?php

namespace CasAuth\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Routing\Router;
use CAS_ProxyChain;
use phpCAS;

class CasAuthenticate extends BaseAuthenticate {

    /**
     * Constructor
     *
     * @param \Cake\Controller\ComponentRegistry $registry The Component registry used on this request.
     * @param array $config Array of config to use.
     */
    public function __construct(ComponentRegistry $registry, array $config = []) {

        parent::__construct($registry, Configure::read('Cas'));

        $this->config($config);

        $settings = $this->config();

        if ($settings['debug']) {
            phpCAS::setDebug($settings['debug_file']);
        }

        if ($settings['proxy_mode']) {
            phpCAS::proxy($settings['version'], $settings['hostname'], $settings['port'], $settings['uri']);
        } else {
            phpCAS::client($settings['version'], $settings['hostname'], $settings['port'], $settings['uri']);
        }

        if (!empty($settings['proxy_list'])) {
            phpCAS::allowProxyChain(new CAS_ProxyChain($settings['proxy_list']));
        }

        //TODO: handle certificate
        phpCAS::setNoCasServerValidation();
    }


    public function authenticate(Request $request, Response $response) {
        return $this->getUser($request);
    }


    public function getUser(Request $request) {
        phpCAS::handleLogoutRequests(false);
        phpCAS::forceAuthentication();

        $user = $this->_findUser(phpCAS::getUser());
        return $user;
    }

    public function logout() {
        if (phpCAS::isAuthenticated()) {

            //TODO: use App.fullBaseUrl
            phpCAS::logout([
                'url' => Router::url('https://cake.test.adullact.org', true),
                'service' => Router::url('https://cake.test.adullact.org', true)
            ]);
        }
    }

    /**
     * Returns a list of all events that this authenticate class will listen to.
     *
     * @return array List of events this class listens to.
     */
    public function implementedEvents() {
        return ['Auth.logout' => 'logout'];
    }

    // TODO: Send a meaningful message
    public function unauthenticated(Request $request, Response $response) {
        throw new ForbiddenException();
    }
}
