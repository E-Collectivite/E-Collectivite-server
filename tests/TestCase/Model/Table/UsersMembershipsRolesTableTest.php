<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersMembershipsRolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersMembershipsRolesTable Test Case
 */
class UsersMembershipsRolesTableTest extends TestCase {

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersMembershipsRolesTable
     */
    public $UsersMembershipsRoles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_memberships_roles',
        'app.roles',
        'app.services',
        'app.connectors',
        'app.subscriptions',
        'app.local_governments',
        'app.users_memberships',
        'app.users',
//        'app.users_memberships_services',
        'app.subscriptions_services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $config = TableRegistry::exists('UsersMembershipsRoles') ? [] : ['className' => 'App\Model\Table\UsersMembershipsRolesTable'];
        $this->UsersMembershipsRoles = TableRegistry::get('UsersMembershipsRoles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->UsersMembershipsRoles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules() {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
