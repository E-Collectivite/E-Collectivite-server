<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LocalGovernmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LocalGovernmentsTable Test Case
 */
class LocalGovernmentsTableTest extends TestCase {

    /**
     * Test subject
     *
     * @var \App\Model\Table\LocalGovernmentsTable
     */
    public $LocalGovernments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
      'app.local_governments',
      'app.subscriptions',
      'app.services',
      'app.connectors',
      'app.subscriptions_services',
      'app.users_memberships',
//      'app.users_memberships_services',
//      'app.pooling_local_governments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $config = TableRegistry::exists('LocalGovernments') ? [] : ['className' => 'App\Model\Table\LocalGovernmentsTable'];
        $this->LocalGovernments = TableRegistry::get('LocalGovernments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->LocalGovernments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules() {
        $this->markTestIncomplete('Not implemented yet.');
    }

}
