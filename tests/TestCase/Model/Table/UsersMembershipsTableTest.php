<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersMembershipsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersMembershipsTable Test Case
 */
class UsersMembershipsTableTest extends TestCase {

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersMembershipsTable
     */
    public $UsersMemberships;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
      'app.users_memberships',
      'app.users',
      'app.local_governments',
      'app.subscriptions',
      'app.services',
      'app.connectors',
      'app.subscriptions_services',
//      'app.users_memberships_services',
//      'app.pooling_local_governments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $config = TableRegistry::exists('UsersMemberships') ? [] : ['className' => 'App\Model\Table\UsersMembershipsTable'];
        $this->UsersMemberships = TableRegistry::get('UsersMemberships', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->UsersMemberships);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules() {
        $this->markTestIncomplete('Not implemented yet.');
    }

}
