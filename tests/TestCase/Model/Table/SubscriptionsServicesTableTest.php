<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubscriptionsServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubscriptionsServicesTable Test Case
 */
class SubscriptionsServicesTableTest extends TestCase {

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubscriptionsServicesTable
     */
    public $SubscriptionsServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
      'app.subscriptions_services',
      'app.subscriptions',
      'app.local_governments',
      'app.services',
      'app.connectors',
      'app.users_memberships',
//      'app.users_memberships_services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $config = TableRegistry::exists('SubscriptionsServices') ? [] : ['className' => 'App\Model\Table\SubscriptionsServicesTable'];
        $this->SubscriptionsServices = TableRegistry::get('SubscriptionsServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->SubscriptionsServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules() {
        $this->markTestIncomplete('Not implemented yet.');
    }

}
