<?php

namespace App\Test\TestCase\Shell;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Shell\UsersShell Test Case
 */
class UsersShellTest extends TestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
    ];

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \App\Shell\UsersShell|\PHPUnit_Framework_MockObject_MockObject
     */
    public $Shell;

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();

        $this->Shell = $this->getMockBuilder('App\Shell\UsersShell')
            ->setMethods(['in', 'out', '_stop', 'clear', 'err', 'success'])
            ->setConstructorArgs([$this->io])
            ->getMock();

        $this->Shell->Users = $this->getMockBuilder('App\Model\Table\UsersTable')
            ->setMethods(['newEntity', 'save'])
            ->getMock();

        $this->Users = TableRegistry::get('Users');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Shell);

        parent::tearDown();
    }

    /**
     * Test getOptionParser method
     *
     * @return void
     */
    public function testGetOptionParser() {
        $this->assertInstanceOf('Cake\Console\ConsoleOptionParser', $this->Shell->getOptionParser());
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * @test
     * When creating a user
     */
    public function whenCreatingAUser() {
        $user = [
            'name' => 'username',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user',
            'superadmin' => false
        ];
        $entity = $this->Users->newEntity($user);
        $this->Shell->Users->expects($this->once())
            ->method('newEntity')
            ->with($user)
            ->will($this->returnValue($entity));

        $this->Shell->Users->expects($this->once())
            ->method('save')
            ->with($entity)
            ->will($this->returnValue($entity));

        $this->Shell->expects($this->once())
            ->method('success')
            ->with('User added:');

        $this->Shell->runCommand(['createUser',
            '--name=' . $user['name'],
            '--password=' . $user['password'],
            '--mail=' . $user['mail'],
            '--firstname=' . $user['firstname'],
            '--lastname=' . $user['lastname'],
            '--superadmin=' . $user['superadmin'],
        ]);
    }

    /**
     * @test
     * When creating a user without parameters
     */
    public function whenCreatingAUserWithoutParameters() {
        $user = [
            'name' => 'username',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'firstname',
            'lastname' => 'lastname',
            'superadmin' => false
        ];

        $this->Shell->expects($this->at(5))->method('in')->will($this->returnValue('username'));
        $this->Shell->expects($this->at(6))->method('in')->will($this->returnValue('password'));
        $this->Shell->expects($this->at(7))->method('in')->will($this->returnValue('mail@example.org'));
        $this->Shell->expects($this->at(8))->method('in')->will($this->returnValue('firstname'));
        $this->Shell->expects($this->at(9))->method('in')->will($this->returnValue('lastname'));
        $this->Shell->expects($this->at(10))->method('in')->will($this->returnValue(false));

        $entity = $this->Users->newEntity($user);
        $this->Shell->Users->expects($this->once())
            ->method('newEntity')
            ->with($user)
            ->will($this->returnValue($entity));

        $this->Shell->Users->expects($this->once())
            ->method('save')
            ->with($entity)
            ->will($this->returnValue($entity));

        $this->Shell->expects($this->once())
            ->method('success')
            ->with('User added:');

        $this->Shell->runCommand(['createUser']);
    }

    /**
     * @test
     * When creating a user without parameters
     */
    public function whenCreatingAUserThatExists() {
        $user = [
            'name' => 'mreyrolle',
            'password' => 'password',
            'mail' => 'not  a mail',
            'firstname' => 'firstname',
            'lastname' => 'lastname',
            'superadmin' => false
        ];

        $entity = $this->Users->newEntity($user);

        $this->Shell->Users->expects($this->once())
            ->method('newEntity')
            ->with($user)
            ->will($this->returnValue($entity));

        $this->Shell->Users->expects($this->once())
            ->method('save')
            ->with($entity)
            ->will($this->returnValue(false));

        $this->Shell->expects($this->once())
            ->method('err')
            ->with('User could not be added:');

        $this->Shell->runCommand(['createUser',
            '--name=' . $user['name'],
            '--password=' . $user['password'],
            '--mail=' . $user['mail'],
            '--firstname=' . $user['firstname'],
            '--lastname=' . $user['lastname'],
            '--superadmin=' . $user['superadmin'],
        ]);
    }
}
