<?php

namespace App\Test\TestCase\Controller\Api\V1;


/**
 * App\Controller\Api\V1\LocalGovernmentsController Test Case
 */
class LocalGovernmentsControllerTest extends ApiIntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.local_governments',
        'app.subscriptions',
        'app.services',
        'app.connectors',
        'app.roles',
        'app.subscriptions_services',
        'app.users_memberships',
        'app.users',
        'app.users_memberships_roles'
    ];
    public $autoFixtures = false;

    public function testWhenGettingAllTheLocalGovernments() {
        $this->loadFixtures('LocalGovernments');
        $this->iSendAGetRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'localGovernments' => [
                [
                    'id' => 1, 'name' => 'ADULLACT', 'siren' => '000000000',
                    'subscription_id' => 1, 'pooling_local_governments_id' => null
                ],
                [
                    'id' => 2, 'name' => 'Montpellier', 'siren' => '000000000',
                    'subscription_id' => 2, 'pooling_local_governments_id' => null
                ]
            ],
            'pagination' => [
                'page_count' => 1,
                'current_page' => 1,
                'has_next_page' => FALSE,
                'has_prev_page' => FALSE,
                'count' => 2,
                'limit' => null
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenGettingNoLocalGovernment() {
        $this->iSendAGetRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'localGovernments' => [],
            'pagination' => [
                'page_count' => 0,
                'current_page' => 1,
                'has_next_page' => FALSE,
                'has_prev_page' => FALSE,
                'count' => 0,
                'limit' => null
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenFindingAnExistingLocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'UsersMemberships');

        $this->iSendAGetRequestTo('/local_governments/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'ADULLACT',
            'siren' => '000000000',
            'subscription_id' => 1,
            'pooling_local_governments_id' => null
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenFindingAnUnknownLocalGovernment() {
        $this->loadFixtures('LocalGovernments');

        $this->iSendAGetRequestTo('/local_governments/10.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 10 does not exist',
            'url' => '/api/v1/local_governments/10.json',
            'code' => 404
        ];
        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingANewLocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = [
            'name' => 'TEST',
            'siren' => '111111118',
            'subscription_id' => 1
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(201);

        $expected = [
            'name' => 'TEST',
            'siren' => '111111118',
            'subscription_id' => 1,
            'id' => 3,
            'pooling_local_governments_id' => null,
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingAnExistingLocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = [
            'name' => 'ADULLACT',
            'siren' => '111111118',
            'subscription_id' => 1
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'name' => [
                    'unique' => 'The provided value is invalid'
                ]
            ],
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingALocalGovernmentWithoutSubscription() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = [
            'name' => 'TEST',
            'siren' => '111111118',
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'subscription_id' => [
                    '_required' => 'This field is required'
                ]
            ],
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingALocalGovernmentWithAnInvalidSiren() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = [
            'name' => 'TEST',
            'siren' => 'invalid',
            'subscription_id' => 1
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments.json',
            'message' => '3 validation errors occurred',
            'errorCount' => 3,
            'errors' => [
                'siren' => [
                    'lengthBetween' => 'The siren needs to be 9 digits long',
                    'numeric' => 'The siren can only contain digits',
                    'valid' => 'The provided value is invalid'
                ]
            ],
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenUpdatingALocalGovernmentThaDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = [
            'siren' => '111111118'
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPutRequestTo('/local_governments/10.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 10 does not exist',
            'url' => '/api/v1/local_governments/10.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenUpdatingTheSirenOfALocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = ['siren' => '111111118'];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPutRequestTo('/local_governments/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'ADULLACT',
            'siren' => '111111118',
            'subscription_id' => 1,
            'pooling_local_governments_id' => null
        ];
        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenUpdatingALocalGovernmentWithASubscriptionThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = ['subscription_id' => 10];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPutRequestTo('/local_governments/1.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments/1.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'subscription_id' => [
                    '_existsIn' => 'This value does not exist'
                ]
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenUpdatingALocalGovernmentWithAPoolingLocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Subscriptions');

        $data = ['pooling_local_governments_id' => 1];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPutRequestTo('/local_governments/2.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 2,
            'name' => 'Montpellier',
            'siren' => '000000000',
            'subscription_id' => 2,
            'pooling_local_governments_id' => 1
        ];
        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenDeletingALocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'UsersMemberships');
        $this->iSendADeleteRequestTo('/local_governments/1.json');
        $this->theResponseCodeShouldBe(204);
    }

    public function testWhenDeletingALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments');
        $this->iSendADeleteRequestTo('/local_governments/15.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When creating a valid new user in a local government that exists
     */
    public function whenCreatingAUser() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'name' => 'username',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user'
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/1/users.json');
        $this->theResponseCodeShouldBe(201);

        $expected = [
            'name' => 'username',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user',
            'superadmin' => false,
            'id' => 3
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When creating a valid new user in a local government that doesn't exist
     */
    public function whenCreatingAUserInALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'name' => 'username',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user'
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/10/users.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 10 does not exist',
            'url' => '/api/v1/local_governments/10/users.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When creating a user that already exists in a loval government that exists
     */
    public function whenCreatingAnExistingUser() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'name' => 'mreyrolle',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user'
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/1/users.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments/1/users.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'name' => [
                    'unique' => 'The provided user already exists'
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When trying to force the creation of a superadmin
     */
    public function whenTryingToCreateASuperadmin() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'name' => 'username',
            'password' => 'password',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user',
            'superadmin' => true
        ];

        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/1/users.json');
        $this->theResponseCodeShouldBe(201);

        $expected = [
            'name' => 'username',
            'mail' => 'mail@example.org',
            'firstname' => 'The firstname of the user',
            'lastname' => 'The lastname of the user',
            'superadmin' => false,
            'id' => 3
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting all members of an existing local government
     */
    public function whenGettingAllTheMembers() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $this->iSendAGetRequestTo('/local_governments/1/members.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'users' => [
                [
                    'id' => 1,
                    'name' => 'mreyrolle',
                    'mail' => 'maxime.reyrolle@example.org',
                    'firstname' => 'Maxime',
                    'lastname' => 'Reyrolle',
                    'superadmin' => false
                ]
            ],
            'pagination' => [
                'page_count' => 1,
                'current_page' => 1,
                'has_next_page' => false,
                'has_prev_page' => false,
                'count' => 1,
                'limit' => null
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting members of a local government that doesn't have any
     */
    public function whenGettingNoMember() {
        $this->loadFixtures('LocalGovernments');

        $this->iSendAGetRequestTo('/local_governments/1/members.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'users' => [
            ],
            'pagination' => [
                'page_count' => 0,
                'current_page' => 1,
                'has_next_page' => false,
                'has_prev_page' => false,
                'count' => 0,
                'limit' => null
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting members of a local government that doesn't exist
     */
    public function whenGettingMembersOfALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments');

        $this->iSendAGetRequestTo('/local_governments/5/members.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 5 does not exist',
            'url' => '/api/v1/local_governments/5/members.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding users to a local government
     */
    public function whenAddingUsersToALocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $data = [
            'users' => [
                [
                    'user_id' => 2,
                    'local_government_administrator' => true
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/2/members.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 2,
            'name' => 'Montpellier',
            'siren' => '000000000',
            'subscription_id' => 2,
            'pooling_local_governments_id' => null,
            'users' => [
                [
                    'id' => 1,
                    'name' => 'mreyrolle',
                    'mail' => 'maxime.reyrolle@example.org',
                    'firstname' => 'Maxime',
                    'lastname' => 'Reyrolle',
                    'superadmin' => false,
                    'local_government_administrator' => false
                ],
                [
                    'id' => 2,
                    'name' => 'aauzolat',
                    'mail' => 'arnaud.auzolat@example.org',
                    'firstname' => 'Arnaud',
                    'lastname' => 'Auzolat',
                    'superadmin' => false,
                    'local_government_administrator' => true
                ],
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * Test when adding users to a local government that doesn't exist
     */
    public function whenAddingUsersToALocalGovernmentThatDOesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $data = [
            'users' => [
                [
                    'user_id' => 2,
                    'local_government_administrator' => true
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/15/members.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/members.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * Test when adding users that don't exist to a local government
     */
    public function whenAddingUsersThatDontExistToALocalGovernment() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $data = [
            'users' => [
                [
                    'user_id' => 4,
                    'local_government_administrator' => false
                ],
                [
                    'user_id' => 5,
                    'local_government_administrator' => false
                ],
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/local_governments/1/members.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The users with the ids 4,5 do not exist',
            'url' => '/api/v1/local_governments/1/members.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting an existing member of an exisiting local government
     */
    public function whenViewingAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $this->iSendAGetRequestTo('/local_governments/1/members/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'mreyrolle',
            'mail' => 'maxime.reyrolle@example.org',
            'firstname' => 'Maxime',
            'lastname' => 'Reyrolle',
            'superadmin' => false,
            'local_government_administrator' => false
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting a member that doesn't exist in an existing local government
     */
    public function whenViewingAMemberThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $this->iSendAGetRequestTo('/local_governments/1/members/9.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 9 does not exist',
            'url' => '/api/v1/local_governments/1/members/9.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting a member of a local government that doesn't exist
     */
    public function whenViewingAMemberOfALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');
        $this->iSendAGetRequestTo('/local_governments/9/members/9.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 9 does not exist',
            'url' => '/api/v1/local_governments/9/members/9.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When editing the firstname and the lastname of a member
     */
    public function whenEditingAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'firstname' => 'new firstname',
            'lastname' => 'new lastname',
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPutRequestTo('/local_governments/1/members/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'mreyrolle',
            'mail' => 'maxime.reyrolle@example.org',
            'firstname' => 'new firstname',
            'lastname' => 'new lastname',
            'superadmin' => false,
            'local_government_administrator' => false
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When editing the role of a member in the local government
     */
    public function whenEditingTheRoleOfAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'local_government_administrator' => true,
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPutRequestTo('/local_governments/1/members/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'mreyrolle',
            'mail' => 'maxime.reyrolle@example.org',
            'firstname' => 'Maxime',
            'lastname' => 'Reyrolle',
            'superadmin' => false,
            'local_government_administrator' => true
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When editing a user that is not member of the local government
     */
    public function whenEditingANonMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'mail' => 'test@test.test'
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPutRequestTo('/local_governments/1/members/2.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 2 does not exist',
            'url' => '/api/v1/local_governments/1/members/2.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When editing a user in a local government that doesn't exist
     */
    public function whenEditingAUserInALocalGovernmentThatDoestNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'mail' => 'test@test.test'
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPutRequestTo('/local_governments/15/members/1.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/members/1.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When editing a user in a local government that doesn't exist
     */
    public function whenEditingAMemberWithABadMail() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $data = [
            'mail' => 'foo'
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPutRequestTo('/local_governments/2/members/1.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/local_governments/2/members/1.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'mail' => [
                    'validFormat' => 'E-mail must be valid'
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When removing a member from the local government
     */
    public function whenRemovingAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Roles', 'UsersMembershipsRoles');

        $this->iSendADeleteRequestTo('/local_governments/1/members/1.json');

        $this->theResponseCodeShouldBe(204);
    }

    /**
     * @test
     * When removing a user not member of the local government from it
     */
    public function whenRemovingANonMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $this->iSendADeleteRequestTo('/local_governments/1/members/2.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 2 does not exist',
            'url' => '/api/v1/local_governments/1/members/2.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When removing a user from a local government that doesn't exist
     */
    public function whenRemovingAUserFromALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $this->iSendADeleteRequestTo('/local_governments/15/members/2.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/members/2.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting roles of a member of the local government
     */
    public function whenViewingRolesOfAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Roles', 'UsersMembershipsRoles');

        $this->iSendAGetRequestTo('/local_governments/1/members/1/roles.json');

        $this->theResponseCodeShouldBe(200);

        $expected = [
            'roles' => [
                [
                    'id' => 1,
                    'name' => 'Agent actes',
                    'service_id' => 1
                ],
                [
                    'id' => 2,
                    'name' => 'Agent helios',
                    'service_id' => 2
                ],
                [
                    'id' => 4,
                    'name' => 'Agent signataire actes',
                    'service_id' => 4
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting roles of a user that is not a member of the local government
     */
    public function whenViewingRolesOfANonMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Roles', 'UsersMembershipsRoles');

        $this->iSendAGetRequestTo('/local_governments/1/members/2/roles.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 2 does not exist',
            'url' => '/api/v1/local_governments/1/members/2/roles.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting roles of a user in a local government that doesn't exist
     */
    public function whenViewingRolesOfAUserInALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships');

        $this->iSendAGetRequestTo('/local_governments/15/members/2/roles.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/members/2/roles.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding roles to a member
     */
    public function whenAddingRolesToMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');
        $data = [
            'roles' => [
                ['role_id' => 1],
                ['role_id' => 2],
            ]
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/local_governments/1/members/1/roles.json');

        $this->theResponseCodeShouldBe(200);

        $expected = [
            'roles' => [
                [
                    'id' => 1,
                    'name' => 'Agent actes',
                    'service_id' => 1
                ],
                [
                    'id' => 2,
                    'name' => 'Agent helios',
                    'service_id' => 2
                ],
                [
                    'id' => 4,
                    'name' => 'Agent signataire actes',
                    'service_id' => 4
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding not available role (exists outside the subscription of the local government) to a member
     */
    public function whenAddingNotAvailableRoleToAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');
        $data = [
            'roles' => [
                ['role_id' => 400],
            ]
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/local_governments/1/members/1/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The role with the id 400 does not exist',
            'url' => '/api/v1/local_governments/1/members/1/roles.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding roles that don't exist (even outside the subscription of the local government) to a member
     */
    public function whenAddingNotExistingRolesToAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');
        $data = [
            'roles' => [
                ['role_id' => 12],
                ['role_id' => 18],
                ['role_id' => 10],
            ]
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/local_governments/1/members/1/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The roles with the ids 12,18,10 do not exist',
            'url' => '/api/v1/local_governments/1/members/1/roles.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding roles to a user that is not member of the local government
     */
    public function whenAddingRolesToANonMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');
        $data = [
            'roles' => [
                ['role_id' => 1],
                ['role_id' => 2],
                ['role_id' => 3],
            ]
        ];

        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/local_governments/1/members/15/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 15 does not exist',
            'url' => '/api/v1/local_governments/1/members/15/roles.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When removing a role to a member
     */
    public function whenRemovingARoleToAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');

        $this->iSendADeleteRequestTo('/local_governments/1/members/1/roles/1.json');

        $this->theResponseCodeShouldBe(204);
    }

    /**
     * @test
     * When removing a role that is not granted to a member
     */
    public function whenRemovingARoleNotAvailableToAMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');

        $this->iSendADeleteRequestTo('/local_governments/1/members/1/roles/3.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The role with the id 3 does not exist',
            'url' => '/api/v1/local_governments/1/members/1/roles/3.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When removing a role to a user that is not member of the local government
     */
    public function whenRemovingARoleToANonMember() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');

        $this->iSendADeleteRequestTo('/local_governments/1/members/15/roles/1.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The user with the id 15 does not exist',
            'url' => '/api/v1/local_governments/1/members/15/roles/1.json',
            'code' => 404

        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When removing a role to a user from a local government that doesn't exist
     */
    public function whenRemovingARoleToAUserFromALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles', 'UsersMembershipsRoles');

        $this->iSendADeleteRequestTo('/local_governments/15/members/1/roles/1.json');

        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/members/1/roles/1.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting available roles
     */
    public function whenGettingAvailableRoles() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles');

        $this->iSendAGetRequestTo('/local_governments/2/roles.json');

        $this->theResponseCodeShouldBe(200);
        $expected = [
            'roles' => [
                [
                    'id' => 4,
                    'name' => 'Agent signataire actes',
                    'service_id' => 4
                ],
                [
                    'id' => 5,
                    'name' => 'Agent signataire helios',
                    'service_id' => 4
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting available roles from a local government that doesn't exist
     */
    public function whenGettingRolesFromALocalGovernmentThatDoesNotExist() {
        $this->loadFixtures('LocalGovernments', 'Users', 'UsersMemberships', 'Connectors', 'Services', 'Subscriptions', 'SubscriptionsServices', 'Roles');

        $this->iSendAGetRequestTo('/local_governments/15/roles.json');

        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The local government with the id 15 does not exist',
            'url' => '/api/v1/local_governments/15/roles.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }
}
