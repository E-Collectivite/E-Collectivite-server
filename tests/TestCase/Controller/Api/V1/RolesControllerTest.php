<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RolesController;

/**
 * App\Controller\RolesController Test Case
 */
class RolesControllerTest extends ApiIntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.roles',
        'app.services',
        'app.connectors',
        'app.subscriptions',
        'app.local_governments',
        'app.users_memberships',
        'app.users',
        'app.subscriptions_services'
    ];
    public $autoFixtures = false;

    /**
     * @test
     * When getting the roles linked to an existing service
     */
    public function whenGettingAllTheRolesOfAnExistingService() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/1/services/1/roles.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            [
                'id' => 1, 'name' => 'Agent actes', 'service_id' => 1
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting no roles
     */
    public function whenGettingNoTheRolesOfAnExistingService() {
        $this->loadFixtures('Connectors', 'Services');
        $this->iSendAGetRequestTo('/connectors/1/services/1/roles.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting roles of a service that doesn't exist
     */
    public function whenGettingRolesOfAServiceThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/1/services/15/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The service with the id 15 does not exist',
            'url' => '/api/v1/connectors/1/services/15/roles.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting roles of a service linked to a connector that doesn't exist
     */
    public function whenGettingRolesOfAServiceLinkedToAConnectorThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/15/services/15/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The connector with the id 15 does not exist',
            'url' => '/api/v1/connectors/15/services/15/roles.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting an existing role of a service that exists
     */
    public function whenGettingARoleOfAnExistingService() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/1/services/1/roles/1.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 1,
            'name' => 'Agent actes',
            'service_id' => 1
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting a role that doesn't exist
     */
    public function whenGettingARoleThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/1/services/1/roles/12.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The role with the id 12 does not exist',
            'url' => '/api/v1/connectors/1/services/1/roles/12.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting a role of a service that doesn't exist
     */
    public function whenGettingARoleOfAServiceThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/1/services/15/roles/1.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The service with the id 15 does not exist',
            'url' => '/api/v1/connectors/1/services/15/roles/1.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When getting a role of a service linked to a connector that doesn't exist
     */
    public function whenGettingARoleOfAServiceLinkedToAConnectorThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendAGetRequestTo('/connectors/15/services/15/roles/1.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The connector with the id 15 does not exist',
            'url' => '/api/v1/connectors/15/services/15/roles/1.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding a new role to an existing service
     */
    public function whenAddingARoleToAService() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $data = [
            'name' => 'Testing role',
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/connectors/1/services/2/roles.json');
        $this->theResponseCodeShouldBe(201);

        $expected = [
            'name' => 'Testing role',
            'service_id' => 2,
            'id' => 6
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding a role that already exists
     */
    public function whenAddingAnExistingRoleToAService() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $data = [
            'name' => 'Agent actes',
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/connectors/1/services/2/roles.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/connectors/1/services/2/roles.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'name' => [
                    'unique' => 'The provided value is invalid'
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding a role to a service that doesn't exist
     */
    public function whenAddingARoleToAServiceThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $data = [
            'name' => 'Agent actes',
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/connectors/1/services/15/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The service with the id 15 does not exist',
            'url' => '/api/v1/connectors/1/services/15/roles.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When adding a role to a service linked to a connector that doesn't exist
     */
    public function whenAddingARoleToAServiceLinkedToAConnectorThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $data = [
            'name' => 'Agent actes',
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/connectors/15/services/15/roles.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The connector with the id 15 does not exist',
            'url' => '/api/v1/connectors/15/services/15/roles.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When deleting an existing role
     */
    public function whenDeletingARole() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendADeleteRequestTo('/connectors/1/services/1/roles/1.json');
        $this->theResponseCodeShouldBe(204);
    }

    /**
     * @test
     * When deleting a role that doesn't exist
     */
    public function whenDeletingARoleThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendADeleteRequestTo('/connectors/1/services/1/roles/15.json');
        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The role with the id 15 does not exist',
            'url' => '/api/v1/connectors/1/services/1/roles/15.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    /**
     * @test
     * When deleting a role of a service that doesn't exist
     */
    public function whenDeletingARoleOfAServiceThatDoesNotExist() {
        $this->loadFixtures('Connectors', 'Services', 'Roles');
        $this->iSendADeleteRequestTo('/connectors/1/services/15/roles/15.json');
        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The service with the id 15 does not exist',
            'url' => '/api/v1/connectors/1/services/15/roles/15.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }
}
