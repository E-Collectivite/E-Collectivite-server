<?php

namespace App\Test\TestCase\Controller\Api\V1;


/**
 * App\Controller\Api\V1\SubscriptionsController Test Case
 */
class SubscriptionsControllerTest extends ApiIntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subscriptions',
        'app.local_governments',
        'app.users_memberships',
        'app.users',
        'app.roles',
        'app.services',
        'app.connectors',
        'app.subscriptions_services',
        'app.users_memberships_roles'
    ];
    public $autoFixtures = false;

    public function testWhenGettingAllSubscriptions() {
        $this->loadFixtures('Subscriptions');
        $this->iSendAGetRequestTo('/subscriptions.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'subscriptions' => [
                [
                    'id' => 1, 'name' => 'EVERYTHING'
                ],
                [
                    'id' => 2, 'name' => 'SIGNATURE'
                ]
            ],
            'pagination' => [
                'page_count' => 1,
                'current_page' => 1,
                'has_next_page' => FALSE,
                'has_prev_page' => FALSE,
                'count' => 2,
                'limit' => null
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);
        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenGettingNoSubscription() {
        $this->iSendAGetRequestTo('/subscriptions.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'subscriptions' => [],
            'pagination' => [
                'page_count' => 0,
                'current_page' => 1,
                'has_next_page' => FALSE,
                'has_prev_page' => FALSE,
                'count' => 0,
                'limit' => null
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenGettingASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendAGetRequestTo('/subscriptions/2.json');
        $this->theResponseCodeShouldBe(200);

        $expected = [
            'id' => 2,
            'name' => 'SIGNATURE',
            'services' => [
                [
                    'id' => 4,
                    'name' => 'signature',
                    'label' => 'Signature',
                    'connector_id' => 2,
                    'class' => null
                ]
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenGettingASubscriptionThatDoesNotExist() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendAGetRequestTo('/subscriptions/5.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The subscription with the id 5 does not exist',
            'url' => '/api/v1/subscriptions/5.json',
            'code' => 404
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingASubscription() {
        $data = [
            'name' => 'test'
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/subscriptions.json');
        $this->theResponseCodeShouldBe(201);

        $expected = [
            'name' => 'test',
            'id' => 1,
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingASubscriptionThatExists() {
        $this->loadFixtures('Connectors', 'Subscriptions');

        $data = [
            'name' => 'SIGNATURE'
        ];
        $this->givenIHaveSomeDataToSend($data);
        $this->iSendAPostRequestTo('/subscriptions.json');
        $this->theResponseCodeShouldBe(400);

        $expected = [
            'code' => 400,
            'url' => '/api/v1/subscriptions.json',
            'message' => 'A validation error occurred',
            'errorCount' => 1,
            'errors' => [
                'name' => [
                    'unique' => "The provided value is invalid"
                ]
            ],
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenDeletingASubscription() {
        $this->loadFixtures('Subscriptions');
        $this->iSendADeleteRequestTo('/subscriptions/1.json');
        $this->theResponseCodeShouldBe(204);
    }

    public function testWhenDeletingASubscriptionThatDoesNotExist() {
        $this->loadFixtures('Subscriptions');
        $this->iSendADeleteRequestTo('/subscriptions/4.json');
        $this->theResponseCodeShouldBe(404);

        $expected = [
            'message' => 'The subscription with the id 4 does not exist',
            'url' => '/api/v1/subscriptions/4.json',
            'code' => 404
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingAServiceToASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 4
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/1/services.json');
        $this->theResponseCodeShouldBe(200);
        $expected = [
            'id' => 1,
            'name' => 'EVERYTHING',
            'services' => [
                [
                    'id' => 1,
                    'name' => 'actes',
                    'label' => 'Actes',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Actes'
                ],
                [
                    'id' => 2,
                    'name' => 'helios',
                    'label' => 'Hélios',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Helios'

                ],
                [
                    'id' => 3,
                    'name' => 'mailsec',
                    'label' => 'Mail sécurisé',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Mailsec'
                ],
                [
                    'id' => 4,
                    'name' => 'signature',
                    'label' => 'Signature',
                    'connector_id' => 2,
                    'class' => null
                ]
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingServicesToASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 1
                ],
                [
                    'service_id' => 2
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/2/services.json');
        $this->theResponseCodeShouldBe(200);
        $expected = [
            'id' => 2,
            'name' => 'SIGNATURE',
            'services' => [
                [
                    'id' => 4,
                    'name' => 'signature',
                    'label' => 'Signature',
                    'connector_id' => 2,
                    'class' => null
                ],
                [
                    'id' => 1,
                    'name' => 'actes',
                    'label' => 'Actes',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Actes'
                ],
                [
                    'id' => 2,
                    'name' => 'helios',
                    'label' => 'Hélios',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Helios'
                ],
            ]
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingServicesThatDoNotExistToASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 10
                ],
                [
                    'service_id' => 20
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/2/services.json');
        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The services with the ids 10,20 do not exist',
            'url' => '/api/v1/subscriptions/2/services.json',
            'code' => 404
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingServicesThatExistsToASubscriptionThatDoesNotExist() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 1
                ],
                [
                    'service_id' => 2
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/3/services.json');
        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The subscription with the id 3 does not exist',
            'url' => '/api/v1/subscriptions/3/services.json',
            'code' => 404
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingServicesThatDoNotExistToASubscriptionThatDoesNotExist() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 10
                ],
                [
                    'service_id' => 20
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/3/services.json');
        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The subscription with the id 3 does not exist',
            'url' => '/api/v1/subscriptions/3/services.json',
            'code' => 404
        ];
        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingAlreadyRelatedServiceToASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 4
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/2/services.json');
        $this->theResponseCodeShouldBe(200);
        $expected = [
            'id' => 2,
            'name' => 'SIGNATURE',
            'services' => [
                [
                    'id' => 4,
                    'name' => 'signature',
                    'label' => 'Signature',
                    'connector_id' => 2,
                    'class' => null
                ]
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenAddingAlreadyRelatedServiceAndNewServicesToASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');

        $data = [
            'services' => [
                [
                    'service_id' => 1
                ],
                [
                    'service_id' => 4
                ]
            ]
        ];
        $this->givenIHaveSomeDataToSend($data);

        $this->iSendAPostRequestTo('/subscriptions/2/services.json');
        $this->theResponseCodeShouldBe(200);
        $expected = [
            'id' => 2,
            'name' => 'SIGNATURE',
            'services' => [
                [
                    'id' => 4,
                    'name' => 'signature',
                    'label' => 'Signature',
                    'connector_id' => 2,
                    'class' => null
                ],
                [
                    'id' => 1,
                    'name' => 'actes',
                    'label' => 'Actes',
                    'connector_id' => 1,
                    'class' => '\App\Model\Logic\Pastell\Actes'
                ],
            ]
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenRemovingAServiceFromASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendADeleteRequestTo('/subscriptions/1/services/2.json');

        $this->theResponseCodeShouldBe(200);
    }

    public function testWhenRemovingAServiceThatDoesNotExistFromASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendADeleteRequestTo('/subscriptions/1/services/20.json');

        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The service with the id 20 does not exist',
            'url' => '/api/v1/subscriptions/1/services/20.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenRemovingAServiceThatExistsButIsNotRelatedFromASubscription() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendADeleteRequestTo('/subscriptions/1/services/400.json');

        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The service with the id 400 does not exist',
            'url' => '/api/v1/subscriptions/1/services/400.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

    public function testWhenRemovingAServiceFromASubscriptionThatDoesNotExist() {
        $this->loadFixtures('Subscriptions', 'Connectors', 'Services', 'SubscriptionsServices');
        $this->iSendADeleteRequestTo('/subscriptions/10/services/1.json');

        $this->theResponseCodeShouldBe(404);
        $expected = [
            'message' => 'The subscription with the id 10 does not exist',
            'url' => '/api/v1/subscriptions/10/services/1.json',
            'code' => 404,
        ];

        $expectedBody = $this->getExpectedValue($expected);

        $this->theResponseBodyShouldBe($expectedBody);
    }

}
