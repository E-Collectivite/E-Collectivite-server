<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LocalGovernmentsFixture
 *
 */
class LocalGovernmentsFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'siren' => ['type' => 'string', 'fixed' => true, 'length' => 9, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null],
        'subscription_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'pooling_local_governments_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_indexes' => [
            'local_governments_fk_local_governments_subscriptions1_idx' => ['type' => 'index', 'columns' => ['subscription_id'], 'length' => []],
            'local_governments_fk_local_governments_local_governments1_idx' => ['type' => 'index', 'columns' => ['pooling_local_governments_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'local_governments_name_UNIQUE' => ['type' => 'unique', 'columns' => ['name'], 'length' => []],
            'fk_pooling_local_governments_id' => ['type' => 'foreign', 'columns' => ['pooling_local_governments_id'], 'references' => ['local_governments', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//        'id' => 1,
            'name' => 'ADULLACT',
            'siren' => '000000000',
            'subscription_id' => 1,
            'pooling_local_governments_id' => null
        ],
        [
            'name' => 'Montpellier',
            'siren' => '000000000',
            'subscription_id' => 2,
            'pooling_local_governments_id' => null
        ]
    ];
}
