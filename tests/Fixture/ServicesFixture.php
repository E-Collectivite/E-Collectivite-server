<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ServicesFixture
 *
 */
class ServicesFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'label' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'connector_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'class' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'services_fk_services_applications1_idx' => ['type' => 'index', 'columns' => ['connector_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'services_name_UNIQUE' => ['type' => 'unique', 'columns' => ['name'], 'length' => []],
            'fk_connector_id' => ['type' => 'foreign', 'columns' => ['connector_id'], 'references' => ['connectors', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'name' => 'actes',
            'label' => 'Actes',
            'connector_id' => 1,
            'class' => '\App\Model\Logic\Pastell\Actes'
        ],
        [
//            'id' => 2,
            'name' => 'helios',
            'label' => 'Hélios',
            'connector_id' => 1,
            'class' => '\App\Model\Logic\Pastell\Helios'
        ],
        [
//            'id' => 3,
            'name' => 'mailsec',
            'label' => 'Mail sécurisé',
            'connector_id' => 1,
            'class' => '\App\Model\Logic\Pastell\Mailsec'
        ],
        [
//            'id' => 4,
            'name' => 'signature',
            'label' => 'Signature',
            'connector_id' => 2,
            'class' => null
        ],
    ];
}
