<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersMembershipsFixture
 *
 */
class UsersMembershipsFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'local_government_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'local_government_administrator' => ['type' => 'boolean', 'length' => null, 'default' => 0, 'null' => false, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'users_memberships_fk_users_has_local_governments_users1_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'users_memberships_fk_users_has_local_governments_local_governme' => ['type' => 'index', 'columns' => ['local_government_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//        'id' => 1,
            'user_id' => 1,
            'local_government_id' => 1,
            'local_government_administrator' => 0
        ],
        [
//        'id' => 2,
            'user_id' => 1,
            'local_government_id' => 2,
            'local_government_administrator' => 0
        ],
    ];
}
