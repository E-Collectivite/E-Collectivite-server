<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubscriptionsServicesFixture
 *
 */
class SubscriptionsServicesFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'subscription_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'service_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_indexes' => [
            'subscriptions_services_fk_subscriptions_has_services_subscripti' => ['type' => 'index', 'columns' => ['subscription_id'], 'length' => []],
            'subscriptions_services_fk_subscriptions_has_services_services1_' => ['type' => 'index', 'columns' => ['service_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['subscription_id', 'service_id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        // Subscription : EVERYTHING
        [
            'subscription_id' => 1,
            'service_id' => 1
        ],
        [
            'subscription_id' => 1,
            'service_id' => 2
        ],
        [
            'subscription_id' => 1,
            'service_id' => 3
        ],
        [
            'subscription_id' => 1,
            'service_id' => 4
        ],
        [
            'subscription_id' => 1,
            'service_id' => 5
        ],
        // Subscription : SIGNATURE
        [
            'subscription_id' => 2,
            'service_id' => 4
        ],
    ];
}
