# E-Collectivité server

[![build status](https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server/badges/develop/build.svg)](https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server/commits/develop)
[![coverage report](https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server/badges/develop/coverage.svg)](https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server/commits/develop)

## Démo

Démo v2: [https://ecollectivite-v2.demonstrations.adullact.org/](https://ecollectivite-v2.demonstrations.adullact.org/)

Démo v1: [http://ecollectivites.demonstrations.adullact.org](http://ecollectivites.demonstrations.adullact.org/)

## FEDER

Projet Cofinancé par le Fonds Européen de Développement Régional

![](Logo_FEDER.png)
