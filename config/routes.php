<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::prefix('api/v1', function (RouteBuilder $routes) {
    $routes->extensions(['json', 'xml']);
    $routes->resources('Users');
    $routes->connect('/users/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/users/logout', ['controller' => 'Users', 'action' => 'logout']);
    $routes->connect('/users/token', ['controller' => 'Users', 'action' => 'token']);

    $routes->resources('Connectors', function (RouteBuilder $routes) {
        $routes->resources('Services', function (RouteBuilder $routes) {
            $routes->resources('Roles');
        });
    });
    $routes->resources('Subscriptions', function (RouteBuilder $routes) {
        $routes->connect('/services', ['controller' => 'Subscriptions', 'action' => 'addServices']);
        $routes->connect('/services/:id', ['controller' => 'Subscriptions', 'action' => 'removeService'], ['id' => '[0-9]+', 'pass' => ['id']]);
    });

    $routes->resources('LocalGovernments', function (RouteBuilder $routes) {
        $routes->scope('/users', function (RouteBuilder $routes) {
            $routes->connect('/', ['controller' => 'LocalGovernments', 'action' => 'createUser', '_method' => 'POST']);
        });

        $routes->scope('/members', function (RouteBuilder $routes) {
            $routes->connect('/', ['controller' => 'LocalGovernments', 'action' => 'getMembers', '_method' => 'GET']);
            $routes->connect('/', ['controller' => 'LocalGovernments', 'action' => 'addMembers', '_method' => 'POST']);
            $routes->connect('/:id', ['controller' => 'LocalGovernments', 'action' => 'viewMember', '_method' => 'GET'], ['id' => '[0-9]+', 'pass' => ['id']]);
            $routes->connect('/:id', ['controller' => 'LocalGovernments', 'action' => 'editMember', '_method' => 'PUT'], ['id' => '[0-9]+', 'pass' => ['id']]);
            $routes->connect('/:id', ['controller' => 'LocalGovernments', 'action' => 'removeMember', '_method' => 'DELETE'], ['id' => '[0-9]+', 'pass' => ['id']]);

            // Cannot scope with an id, scope strings has to be static
            $routes->connect('/:id/roles', ['controller' => 'LocalGovernments', 'action' => 'viewRolesOfMember', '_method' => 'GET'], ['id' => '[0-9]+', 'pass' => ['id']]);
            $routes->connect('/:id/roles', ['controller' => 'LocalGovernments', 'action' => 'addRolesToMember', '_method' => 'POST'], ['id' => '[0-9]+', 'pass' => ['id']]);
            $routes->connect('/:user_id/roles/:role_id', ['controller' => 'UsersMembershipsRoles', 'action' => 'deleteRole', '_method' => 'DELETE'], ['role_id' => '[0-9]+','user_id' => '[0-9]+', 'pass' => ['role_id']]);

        });
    });
    //Get services data
    $routes->connect('/local_governments/:local_government_id/services', ['controller' => 'LocalGovernments', 'action' => 'getServicesData', '_method' => 'GET'], ['local_government_id' => '[0-9]+']);
    // Get service data
    $routes->connect('/local_governments/:local_government_id/services/:service_id', ['controller' => 'LocalGovernments', 'action' => 'getServiceData', '_method' => 'GET'], ['local_government_id' => '[0-9]+', 'service_id' => '[0-9]+', 'pass' => ['service_id']]);
    // Get available roles
    $routes->connect('/local_governments/:local_government_id/roles', ['controller' => 'LocalGovernments', 'action' => 'getRoles', '_method' => 'GET'], ['local_government_id' => '[0-9]+']);
});

Router::scope('/', function (RouteBuilder $routes) {
    $routes->extensions(['json', 'xml']);

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
