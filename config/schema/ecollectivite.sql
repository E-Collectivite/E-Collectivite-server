--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.7
-- Dumped by pg_dump version 9.5.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: connectors; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE connectors (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    url character varying(425) NOT NULL,
    label character varying(255) NOT NULL
);


ALTER TABLE connectors OWNER TO ecollectivite;

--
-- Name: connectors_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE connectors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE connectors_id_seq OWNER TO ecollectivite;

--
-- Name: connectors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE connectors_id_seq OWNED BY connectors.id;


--
-- Name: local_governments; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE local_governments (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    siren character(9) NOT NULL,
    subscription_id integer NOT NULL,
    pooling_local_governments_id integer
);


ALTER TABLE local_governments OWNER TO ecollectivite;

--
-- Name: local_governments_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE local_governments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE local_governments_id_seq OWNER TO ecollectivite;

--
-- Name: local_governments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE local_governments_id_seq OWNED BY local_governments.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255),
    service_id integer NOT NULL
);


ALTER TABLE roles OWNER TO ecollectivite;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO ecollectivite;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE services (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    connector_id integer NOT NULL,
    class character varying(255),
    label character varying(255) NOT NULL
);


ALTER TABLE services OWNER TO ecollectivite;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE services_id_seq OWNER TO ecollectivite;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE services_id_seq OWNED BY services.id;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE subscriptions (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE subscriptions OWNER TO ecollectivite;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subscriptions_id_seq OWNER TO ecollectivite;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE subscriptions_id_seq OWNED BY subscriptions.id;


--
-- Name: subscriptions_services; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE subscriptions_services (
    subscription_id integer NOT NULL,
    service_id integer NOT NULL
);


ALTER TABLE subscriptions_services OWNER TO ecollectivite;

--
-- Name: users; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    mail character varying(255) NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    superadmin boolean DEFAULT false NOT NULL
);


ALTER TABLE users OWNER TO ecollectivite;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO ecollectivite;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_memberships; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE users_memberships (
    id integer NOT NULL,
    user_id integer NOT NULL,
    local_government_id integer NOT NULL,
    local_government_administrator boolean DEFAULT false NOT NULL
);


ALTER TABLE users_memberships OWNER TO ecollectivite;

--
-- Name: users_memberships_id_seq; Type: SEQUENCE; Schema: public; Owner: ecollectivite
--

CREATE SEQUENCE users_memberships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_memberships_id_seq OWNER TO ecollectivite;

--
-- Name: users_memberships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ecollectivite
--

ALTER SEQUENCE users_memberships_id_seq OWNED BY users_memberships.id;


--
-- Name: users_memberships_roles; Type: TABLE; Schema: public; Owner: ecollectivite
--

CREATE TABLE users_memberships_roles (
    role_id integer NOT NULL,
    user_membership_id integer NOT NULL
);


ALTER TABLE users_memberships_roles OWNER TO ecollectivite;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY connectors ALTER COLUMN id SET DEFAULT nextval('connectors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY local_governments ALTER COLUMN id SET DEFAULT nextval('local_governments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY services ALTER COLUMN id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY subscriptions ALTER COLUMN id SET DEFAULT nextval('subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships ALTER COLUMN id SET DEFAULT nextval('users_memberships_id_seq'::regclass);


--
-- Name: connectors_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY connectors
    ADD CONSTRAINT connectors_pkey PRIMARY KEY (id);


--
-- Name: local_governments_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY local_governments
    ADD CONSTRAINT local_governments_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_services_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY subscriptions_services
    ADD CONSTRAINT subscriptions_services_pkey PRIMARY KEY (subscription_id, service_id);


--
-- Name: users_memberships_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships
    ADD CONSTRAINT users_memberships_pkey PRIMARY KEY (id);


--
-- Name: users_memberships_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships_roles
    ADD CONSTRAINT users_memberships_roles_pkey PRIMARY KEY (role_id, user_membership_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: connectors_name_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "connectors_name_UNIQUE" ON connectors USING btree (name);


--
-- Name: local_governments_fk_local_governments_local_governments1_idx; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX local_governments_fk_local_governments_local_governments1_idx ON local_governments USING btree (pooling_local_governments_id);


--
-- Name: local_governments_fk_local_governments_subscriptions1_idx; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX local_governments_fk_local_governments_subscriptions1_idx ON local_governments USING btree (subscription_id);


--
-- Name: local_governments_name_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "local_governments_name_UNIQUE" ON local_governments USING btree (name);


--
-- Name: services_fk_services_applications1_idx; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX services_fk_services_applications1_idx ON services USING btree (connector_id);


--
-- Name: services_name_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "services_name_UNIQUE" ON services USING btree (name);


--
-- Name: subscriptions_name_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "subscriptions_name_UNIQUE" ON subscriptions USING btree (name);


--
-- Name: subscriptions_services_fk_subscriptions_has_services_services1_; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX subscriptions_services_fk_subscriptions_has_services_services1_ ON subscriptions_services USING btree (service_id);


--
-- Name: subscriptions_services_fk_subscriptions_has_services_subscripti; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX subscriptions_services_fk_subscriptions_has_services_subscripti ON subscriptions_services USING btree (subscription_id);


--
-- Name: users_email_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "users_email_UNIQUE" ON users USING btree (mail);


--
-- Name: users_memberships_fk_users_has_local_governments_local_governme; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX users_memberships_fk_users_has_local_governments_local_governme ON users_memberships USING btree (local_government_id);


--
-- Name: users_memberships_fk_users_has_local_governments_users1_idx; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE INDEX users_memberships_fk_users_has_local_governments_users1_idx ON users_memberships USING btree (user_id);


--
-- Name: users_name_UNIQUE; Type: INDEX; Schema: public; Owner: ecollectivite
--

CREATE UNIQUE INDEX "users_name_UNIQUE" ON users USING btree (name);


--
-- Name: fk_connector_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY services
    ADD CONSTRAINT fk_connector_id FOREIGN KEY (connector_id) REFERENCES connectors(id);


--
-- Name: fk_local_government_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships
    ADD CONSTRAINT fk_local_government_id FOREIGN KEY (local_government_id) REFERENCES local_governments(id);


--
-- Name: fk_pooling_local_governments_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY local_governments
    ADD CONSTRAINT fk_pooling_local_governments_id FOREIGN KEY (pooling_local_governments_id) REFERENCES local_governments(id);


--
-- Name: fk_role_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships_roles
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: fk_roles_service_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT fk_roles_service_id FOREIGN KEY (service_id) REFERENCES services(id);


--
-- Name: fk_service_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY subscriptions_services
    ADD CONSTRAINT fk_service_id FOREIGN KEY (service_id) REFERENCES services(id);


--
-- Name: fk_subscription_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY local_governments
    ADD CONSTRAINT fk_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions(id);


--
-- Name: fk_subscription_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY subscriptions_services
    ADD CONSTRAINT fk_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions(id);


--
-- Name: fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_user_membership_id; Type: FK CONSTRAINT; Schema: public; Owner: ecollectivite
--

ALTER TABLE ONLY users_memberships_roles
    ADD CONSTRAINT fk_user_membership_id FOREIGN KEY (user_membership_id) REFERENCES users_memberships(id);


--
-- PostgreSQL database dump complete
--
