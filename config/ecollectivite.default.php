<?php

// Clone this file as "ecollectivite.php"

use App\Model\Logic\Provisioning\Strategy\Parapheur\SignatureActesStrategy;
use App\Model\Logic\Provisioning\Strategy\Parapheur\SignatureHeliosStrategy;
use App\Model\Logic\Provisioning\Strategy\Pastell\ActesSignatureStrategy;
use App\Model\Logic\Provisioning\Strategy\Pastell\HeliosSignatureStrategy;
use App\Model\Logic\Provisioning\Strategy\Pastell\MailsecStrategy;
use App\Model\Logic\Provisioning\Strategy\Pastell\TdtActesStrategy;
use App\Model\Logic\Provisioning\Strategy\Pastell\TdtHeliosStrategy;
use Cake\Core\Configure;

Configure::write([

    'ecollectivite' => [
        'client_url' => '__ECOLLECTIVITE_CLIENT_URL__',
    ],

    /**
     * Configure basic information about the connectors and services.
     */
    'pastell' => [
        'auth' => [
            'username' => '__PASTELL_USERNAME__',
            'password' => '__PASTELL_PASSWORD__'
        ],
        'connectors' => [
            's2low' => [
                'url' => '__TDT_URL__',
                'authentication_for_teletransmisson' => true,
                'server_certificate' => '__PASTELL_SERVER_CERTIFICATE__',
                'user_certificat' => '__PASTELL_USER_CERTIFICATE__',
                'user_certificat_password' => '__PASTELL_USER_CERTIFICATE_PASSWORD__'
            ],
            'iParapheur' => [
                'iparapheur_activate' => true,
                'iparapheur_wsdl' => '__IPARAPHEUR_WSDL__',
                // path to the certificate (p12 format)
                'iparapheur_user_certificat' => '__IPARAPHEUR_USER_CERTIFICATE__',
                'iparapheur_user_certificat_password' => '__IPARAPHEUR_USER_CERTIFICATE_PASSWORD__',
//                'iparapheur_login' => '',
//                'iparapheur_password' => '',
            ]
        ]
    ],
    'parapheur' => [
        'auth' => [
            'username' => '__IPARAPHEUR_USERNAME__',
            'password' => '__IPARAPHEUR_PASSWORD__'
        ]
    ],


    /**
     * Configure the match between E-Collectivité roles and those from distant application
     */
    'Roles' => [
        'pastell' => [
            'Agent actes' => 'actes',
            'Agent helios' => 'helios',
            'Agent mail sécurisé' => 'mailsec'
        ],
        'parapheur' => [
            'Agent signataire actes' => 'Actes',
            'Agent signataire helios' => 'Helios'
        ]
    ],

    /**
     * Configure the strategies used to automate the configuration of local governments in distant application
     */
    'Strategies' => [
        'pastell' => [
            [
                'expression' => 'mailsec',
                'strategy' => new MailsecStrategy()
            ],
            [
                'expression' => 'actes',
                'strategy' => new TdtActesStrategy()
            ],
            [
                'expression' => 'helios',
                'strategy' => new TdtHeliosStrategy()
            ],
            [
                'expression' => 'helios AND signature',
                'strategy' => new HeliosSignatureStrategy()
            ],
            [
                'expression' => 'actes AND signature',
                'strategy' => new ActesSignatureStrategy()
            ],
        ],
        'parapheur' => [
            [
                'expression' => 'actes AND signature',
                'strategy' => new SignatureActesStrategy()
            ],
            [
                'expression' => 'helios AND signature',
                'strategy' => new SignatureHeliosStrategy()
            ]
        ]
    ],

    /**
     * Configure what the API should respond depending on the service requested
     */
    'Filters' => [
        'pastell' => [
            'actes' => [
                'erreur-verif-tdt' => [
                    'singular' => '{0} document with TDT error',
                    'plural' => '{0} documents with TDT errors'
                ],
                'modification' => [
                    'singular' => '{0} pending document',
                    'plural' => '{0} pending documents'
                ],
                'verif-reponse-tdt' => [
                    'singular' => '{0} document with prefecture response',
                    'plural' => '{0} documents with prefecture response'
                ]
            ],
            'helios' => [
                'tdt-error' => [
                    'singular' => '{0} document with TDT error',
                    'plural' => '{0} documents with TDT errors'
                ],
                'recu-iparapheur' => [
                    'singular' => '{0} document to be transmitted to the TDT',
                    'plural' => '{0} documents to be transmitted to the TDT'
                ],
                'send-tdt' => [
                    'singular' => '{0} document sent',
                    'plural' => '{0} documents sent'
                ]
            ],
            'mailsec' => [
                'envoi' => [
                    'singular' => '{0} unread email',
                    'plural' => '{0} unread emails'
                ],
                'reception-partielle' => [
                    'singular' => '{0} partially read email',
                    'plural' => '{0} partially read emails'
                ],
                'reception' => [
                    'singular' => '{0} received email',
                    'plural' => '{0} received emails'
                ]
            ]
        ],
        'parapheur' => [
            'a-traiter' => [
                'singular' => '{0} file to treat',
                'plural' => '{0} files to treat'
            ],
            'en-retard' => [
                'singular' => '{0} file late',
                'plural' => '{0} files late'
            ]
        ]
    ]
]);
