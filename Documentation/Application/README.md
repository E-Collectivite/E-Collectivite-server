# E-COLLECTIVITÉ - Manuel utilisateur

## Admin de collectivité

Les administrateurs de collectivité doivent **toujours** créer leurs objets
depuis le portail E-Collectivité, jamais directement depuis une application métier
comme Pastell ou i-Parapheur.

En l'espèce, E-Collectivité créera automatiquement dans le Parapheur des bureaux 
selon diverses nomenclatures. Afin de garantir le bon fonctionnement, il est impértif
de ne **pas** créer de bureaux manuellement.

## Création d'une collectivité

Juste après la Création d'une collectivité par un superadmin, l'admin de collectivité
devra configurer :

* les informations de certificat dans Pastell (afin de rendre fonctionnels les connecteurs TdT, Tier de Télétransmission)
