# Configuration clé en main - NOTES

## Expression de besoin (copier/coller du TODO)

* Écrire le besoin: ne pas devoir faire de config supplémentaire des services à la livraison de E-Collectivités
* Pour une petite collectivité, identifier les rôles pour:
    * Parapheur
    * Actes
    * Hélios
    * Mail sécurisé
* Pour une petite collectivité, et pour satisfaire les rôles identifiés, comment configurer:
    * Parapheur
    * Actes
    * Hélios
    * Mail sécurisé
* Lister le travail à faire
* MAJ macro-planing

## Notes

### Correspondance Rôles EColl / permissions de l'appli cible

| Rôles E-Collectivités | Application | Permissions              |
|:----------------------|:------------|:-------------------------|
| Agent actes           | Pastell     | entite:lecture           |
|                       |             | journal:lecture          |
|                       |             | actes-generique:edition  |
|                       |             | actes-generique:lecture  |
| Agent helios          | Pastell     | entite:lecture           |
|                       |             | journal:lecture          |
|                       |             | helios-generique:edition |
|                       |             | helios-generique:lecture |
| Agent mail sécurisé   | Pastell     | entite:lecture           |
|                       |             | journal:lecture          |
|                       |             | mailsec:edition          |
|                       |             | mailsec:lecture          |
| Agent signataire Actes    | Parapheur   | bureau <Collectivité>-Actes    |
| Agent signataire Helios   | Parapheur   | bureau <Collectivité>-Helios   |

## Comportements selon les cas (Règles métiers et config clé en main)

Lors de la création d'une collectivité, des actions différentes sont opérées selon les services auxquels elle a accès

### Pré-requis

Les pré-requis suivants doivent être réalisés à l'installation du parapheur.

* L'existence du circuit "Chef de"
    * Etape : action:Signature, concerne le bureau "Chef de"
* L'existence des types "ACTES" et "PES" sur le parapheur (PES = Hélios = Protocole d'Échange Standard)
* L'existence du sous-type "ACTES-Signature"
    * Visibilité en filtre "public"
    * Circuit de validation : "Chef de"
* L'existence du sous-type "HELIOS-Signature"
    * Visibilité en filtre "public"
    * Circuit de validation : "Chef de"

### _ACTES_ ou _HELIOS_

* un connecteur "s2low" de la famille "TdT" est créé et configuré sur Pastell pour cette collectivité. Une intervention humaine reste requise pour ajouter le certificat et son mot de passe.

### _MAIL SÉCURISÉ_

* un connecteur "mailsec" est créé sur Pastell pour cette collectivité

### _ACTES_ et _SIGNATURE_

Dans Iparapheur :

* Un bureau `<Collectivité>-Actes` est créé sur le Parapheur
    * Habilitations : traiter des dossiers
* Un sous-bureau `<Collectivité>-Actes-Pastell` est créé ayant pour supérieur hiérarchique le bureau `<Collectivité>-Actes`
    * Habilitations : créer des dossiers
* Mettre à jour le sous-type "ACTES-Signature" (cf pré requis)
    * Permission de création : ajouter le bureau `<Collectivité>-Actes-Pastell`
* Un utilisateur technique "Actes" est créé sur le parapheur uniquement (ie: pastell-actes@collectivité)
* L'utilisateur technique (pastell-actes@collectivité) devient propriétaire du bureau "<Collectivité>-Actes-Pastell"

Dans Pastell :

* Créer le connecteur "iparapheur - ACTES" sur Pastell et le configurer :
   * Login = pastell-actes@collectivité
   * Type = Actes
   * URL = URL WSDL
   * Certificat utilisateur (RGS-1* serveur / client ; le même pour tout le monde)

### _HELIOS_ et _SIGNATURE_

Dans Iparapheur :

* Un bureau `<Collectivité>-Helios` est créé sur le Parapheur
    * Habilitations : traiter des dossiers
* Un sous-bureau `<Collectivité>-Helios-Pastell` est créé ayant pour supérieur hiérarchique le bureau `<Collectivité>-Helios`
    * Habilitations : créer des dossiers
* Mettre à jour le sous-type "Helios-Signature" (cf pré requis)
    * Permission de création : ajouter le bureau `<Collectivité>-Helios-Pastell`
* Un utilisateur technique "Helios" est créé sur le parapheur uniquement (ie: pastell-Helios@collectivité)
* L'utilisateur technique (pastell-Helios@collectivité) devient propriétaire du bureau "<Collectivité>-Helios-Pastell"

Dans Pastell :

* Créer le connecteur "iparapheur - Helios" sur Pastell et le configurer :
   * Login = pastell-Helios@collectivité
   * Type = Helios
   * URL = URL WSDL
   * Certificat utilisateur (RGS-1* serveur / client ; le même pour tout le monde)

