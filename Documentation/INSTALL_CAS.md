# CAS server Installation

## Packages

```bash
apt-get install \
    tomcat8 \
    apache2
```

## Apache configuration

### Required modules

```bash
a2enmod ssl rewrite proxy proxy_ajp
```

### Virtual host example

```text
<VirtualHost *:80> 
	ServerName cas.example.com 
	Redirect / https://cas.example.com/ 
</VirtualHost> 

<VirtualHost *:443> 
	ServerName cas.example.com 
	Redirect / https://cas.example.com/cas/ 

	SSLEngine On 
	SSLProtocol All -SSLv2 -SSLv3
	SSLCipherSuite EECDH+AES:+AES128:+AES256:+SHA
	SSLCertificateFile /etc/apache2/ssl/wildcard.example.com.pem 
	SSLCertificateKeyFile /etc/apache2/ssl/wildcard.example.com.key 
	SSLCACertificateFile /etc/apache2/ssl/cacert.pem 

	LogLevel warn 
	ErrorLog /var/log/apache2/cas-error.log 
	CustomLog /var/log/apache2/cas-access.log combined 

	<Proxy *> 
		AddDefaultCharset Off 
		Order deny,allow 
		Allow from all 
	</Proxy> 

	ProxyPass /cas ajp://localhost:8009/cas 
	ProxyPassReverse /cas ajp://localhost:8009/cas 
</VirtualHost>
```

## Tomcat configuration

### AJP connector

In the file `/var/lib/tomcat8/conf/server.xml` uncomment the line `<Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />`

### Setup CAS webapp

(TODO: simplify the deployment)

```bash
git clone https://github.com/apereo/cas-overlay-template.git
cd cas-overlay-template
```

Modify the `pom.xml` to add the ldap dependency

```text
            <dependency>
    			<groupId>org.apereo.cas</groupId>
    			<artifactId>cas-server-support-ldap</artifactId>
    			<version>${cas.version}</version>
    		</dependency>
```

Modify the `etc/cas/config/cas.properties` file with the following information (change cas server name and ldap credentials if needed)

```text
##
# CAS Authentication Credentials
#

# Disable static authentication
cas.authn.accept.users=

logging.config=file:/etc/cas/config/log4j2.xml

cas.server.name: https://cas.example.com
cas.server.prefix: https://cas.example.com/cas

cas.adminPagesSecurity.ip=127\.0\.0\.1

cas.authn.ldap[0].ldapUrl=ldap://127.0.0.1:389
cas.authn.ldap[0].useSsl=false
cas.authn.ldap[0].useStartTls=false
cas.authn.ldap[0].connectTimeout=5000
cas.authn.ldap[0].baseDn=ou=People,dc=ecollectivite,dc=org
cas.authn.ldap[0].userFilter=(uid={user})
cas.authn.ldap[0].bindDn=uid=ldapreader,ou=System,dc=ecollectivite,dc=org
cas.authn.ldap[0].bindCredential=<password>

cas.authn.ldap[0].dnFormat=uid=%s,ou=People,dc=ecollectivite,dc=org
cas.authn.ldap[0].principalAttributeId=uid
cas.authn.ldap[0].principalAttributePassword=

cas.authn.ldap[0].type=AUTHENTICATED

cas.authn.ldap[0].minPoolSize=3
cas.authn.ldap[0].maxPoolSize=10
cas.authn.ldap[0].validateOnCheckout=true
cas.authn.ldap[0].validatePeriodically=true
cas.authn.ldap[0].validatePeriod=600

cas.authn.ldap[0].failFast=true
cas.authn.ldap[0].idleTime=5000
cas.authn.ldap[0].prunePeriod=5000
cas.authn.ldap[0].blockWaitTime=5000
```

####  Proxy policy

```bash
mkdir -p src/main/webapp/WEB-INF/classes/services
vim src/main/webapp/WEB-INF/classes/services/HTTPS_PROXY.json
```

```text
{
  "@class" : "org.apereo.cas.services.RegexRegisteredService",
  "serviceId" : "^https://.*",
  "name" : "HTTPS PROXY",
  "id" : 10000003,
  "description" : "This service definition authorizes all application urls that support HTTPS and IMAPS protocols.",
  "proxyPolicy" : {
    "@class" : "org.apereo.cas.services.RegexMatchingRegisteredServiceProxyPolicy",
  	"pattern" : "^https://.*"
  },
  "evaluationOrder" : 0,
  "logoutType" : "BACK_CHANNEL",
  "usernameAttributeProvider" : {
    "@class" : "org.apereo.cas.services.DefaultRegisteredServiceUsernameProvider"
  },
  "attributeReleasePolicy" : {
    "@class" : "org.apereo.cas.services.ReturnAllowedAttributeReleasePolicy"
  },
  "accessStrategy" : {
    "@class" : "org.apereo.cas.services.DefaultRegisteredServiceAccessStrategy",
    "enabled" : true,
    "ssoEnabled" : true
  }
}
```

#### Self signed certificate

If the certificate is self signed, add it to the trusted ones of the JVM

```bash
keytool -import -alias CAS -file /etc/apache2/ssl/wildcard.example.com.pem -keystore /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts
```
