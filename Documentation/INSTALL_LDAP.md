# Basic OpenLDAP Installation

## Packages

```bash
apt-get install slapd ldap-utils
```

## Reconfigure OpenLDAP server

```bash
dpkg-reconfigure slapd
```

You'll be asked a series of questions to configure the server, here is an example for the base DN `ecollectivite.org` :

(No > ecollectivite.org > E-Collectivite > MDB > No > Yes > No)

## Import basic schema

```bash
ldapadd -x -v -D cn=admin,dc=ecollectivite,dc=org -W -f /tmp/init.ldif
```

(TODO: include `init.ldif` in the project)

## ACL

TODO
