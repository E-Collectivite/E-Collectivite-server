# E-Collectivite-server Installation

## Prerequisites

### Packages for Apache, PHP, Postgres

```bash
apt-get install \
    zip \
    unzip \
    apache2 \
    php7.0 \
    php7.0-intl \
    php7.0-mbstring \
    php7.0-xml \
    php7.0-curl \
    libapache2-mod-php7.0 \
    postgresql-9.5 \
    php7.0-pgsql \
    php7.0-ldap
```

### Composer

```bash
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
```

## Postgres authentication

```bash
sed -i -e '/local *all *all/{s/peer/md5/}' /etc/postgresql/9.5/main/pg_hba.conf
```

and restart Postgres

```bash
systemctl restart postgresql
```

## Create Postgres user and database

```bash
sudo -u postgres psql
```

```sql
CREATE USER ecollectivite WITH PASSWORD '<PASSWORD>';
CREATE DATABASE ecollectivite;
GRANT ALL PRIVILEGES ON DATABASE ecollectivite to ecollectivite;
```

## Setup E-Collectivité

```bash
useradd -m ecollectivite
```

(TODO: have a downloadable release)
```bash
su ecollectivite
cd /home/ecollectivite/
git clone https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server.git
git checkout develop
cd E-Collectivite-server/
composer install
cd config
cp ecollectivite.default.php ecollectivite.php
cp ldap.default.php ldap.php
exit
chown ecollectivite:www-data -R .
```

Create the `config/cas.php` file with the following data and change the hostname to match your domain

```php
<?php

return [
    'Cas' => [
      'version' => '3.0',
      'hostname' => 'cas-ecollectivite.example.com',
      'port' => 443,
      'uri' => '/cas',
      'proxy_mode' => true,
      'proxy_list' => [],
      'debug' => true,
      'debug_file' => LOGS . 'cas.log',
    ]
];
```

## Setup database

Edit the `config/app.php` file to change the database user, password and name at the appropriate location (Datasources > default)

Import the schema
```bash
psql -U ecollectivite -W ecollectivite -f config/schema/ecollectivite.sql
```

## Setup apache

### Enable required modules

```bash
a2enmod ssl rewrite
```

### Virtual host example

```text
<VirtualHost *:80> 
ServerName ecollectivite.example.com
Redirect / https://ecollectivite.example.com/ 
</VirtualHost> 

<VirtualHost *:443> 
ServerName ecollectivite.example.com
DocumentRoot /var/www/html/ecollectivite.example.com/webroot 

SSLEngine On 
SSLProtocol All -SSLv2 -SSLv3 
SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH 
SSLCertificateFile /etc/apache2/ssl/wildcard.example.com.pem 
SSLCertificateKeyFile /etc/apache2/ssl/wildcard.example.com.key 
SSLCACertificateFile /etc/apache2/ssl/cacert.pem

LogLevel warn
ErrorLog /var/log/apache2/ecollectivite-error.log
CustomLog /var/log/apache2/ecollectivite-access.log combined

<Directory /home/ecollectivite/E-Collectivite-server/webroot>
	Options Indexes FollowSymLinks MultiViews
	AllowOverride All
	Require all granted
</Directory>
</VirtualHost>
```
