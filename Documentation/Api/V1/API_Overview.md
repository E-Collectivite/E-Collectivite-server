# Summary of the API

/api/v1

## Administration API

Equivalences between CRUD operations & HTTP methods

| HTTP METHOD | CRUD operation |
|:------------|:---------------|
| POST        | CREATE         |
| GET         | READ           |
| PUT         | UPDATE         |
| DELETE      | DELETE         |

**Note**: empty cells means respond with HTTP code 405 "method not available"

Remaining questions:

* Should we implement bulk delete operations (example: delete 5 users at once). And if so, how ?
    * Create a custom POST operation.
    * Create a collection with a custom POST operation then delete it with a custom DELETE operation.
    * Documentation about this question :
        * https://stackoverflow.com/questions/21863326/delete-multiple-records-using-rest
        * https://stackoverflow.com/questions/2421595/restful-way-for-deleting-a-bunch-of-items
        * https://stackoverflow.com/questions/32649651/how-to-delete-list-of-items-with-rest-api

### Users

| HTTP METHOD | POST            | GET        | PUT         | DELETE      |
|:------------|:----------------|:-----------|:------------|:------------|
| /users      | Create new user | List users |             |             |
| /users/{id} |                 | Show {id}  | Update {id} | Delete {id} |

### Local Governments

| HTTP METHOD                                            | POST                        | GET                    | PUT              | DELETE                                      |
|:-------------------------------------------------------|:----------------------------|:-----------------------|:-----------------|:--------------------------------------------|
| /local_governments                                     | Create new local_government | List local_governments |                  |                                             |
| /local_governments/{id}                                |                             | Show {id}              | Update {id}      | Delete {id}                                 |
| /local_governments/{id}/users                          | Create user to {id}         |                        |                  |                                             |
| /local_governments/{id}/members                        | Add existing user to {id}   | List members from {id} |                  |                                             |
| /local_governments/{id}/members/{user_id}              |                             | Show {user_id}         | Update {user_id} | Remove {user_id} from local_government {id} |
| /local_governments/{id}/members/{user_id}/roles        | Add role(s) to {user_id}    | Show {user_id}'s roles |                  |                                             |
| /local_governments/{id}/members/{user_id}/roles/{role} |                             |                        |                  | Remove {role} of {user_id}                  |

### Subscriptions

| HTTP METHOD                               | POST                    | GET                     | PUT | DELETE                        |
|:------------------------------------------|:------------------------|:------------------------|:----|:------------------------------|
| /subscriptions                            | Create new subscription | List subscriptions      |     |                               |
| /subscriptions/{id}                       |                         | Show {id}               |     | Delete {id}                   |
| /subscriptions/{id}/services              | Add service(s) to {id}  | List services from {id} |     |                               |
| /subscriptions/{id}/services/{service_id} |                         | Show {service_id}       |     | Remove {service_id} from {id} |

### Connectors

| HTTP METHOD                            | POST                 | GET                             | PUT         | DELETE              |
|:---------------------------------------|:---------------------|:--------------------------------|:------------|:--------------------|
| /connectors                            | Create new connector | List connectors                 |             |                     |
| /connectors/{id}                       |                      | Show {id}                       | Update {id} | Delete {id}         |
| /connectors/{id}/services              | Create new service   | List services connected to {id} |             |                     |
| /connectors/{id}/services/{service_id} |                      | Show {service_id}               |             | Delete {service_id} |

## User API

This endpoint is available to any **authenticated** user (as long as he is member of the local government and has access to the targeted service)

| HTTP METHOD                                   | POST | GET                                                                      | PUT | DELETE |
|:----------------------------------------------|:-----|:-------------------------------------------------------------------------|:----|:-------|
| /local_governments/{id}/services/{service_id} |      | Show data of {service_id} of local_government {id} to authenticated user |     |        |

# Notes on API

## PUT /connectors/{id}/services/{service_id}

* not implemented yet (maybe one day, if we need to update the name of the service)

## PUT /subscriptions/{id}/services/{service_id}

* It is not the subscription responsibility to update a service. This is the connector's responsibility

## PUT /subscriptions/{id}

* not implemented yet (maybe one day, if we need to update the name of the subscription)
