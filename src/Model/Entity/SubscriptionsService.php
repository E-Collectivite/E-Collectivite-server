<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubscriptionsService Entity.
 *
 * @property int $subscription_id
 * @property \App\Model\Entity\Subscription $subscription
 * @property int $service_id
 * @property \App\Model\Entity\Service $service
 */
class SubscriptionsService extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
      '*' => true,
      'subscription_id' => false,
      'service_id' => false,
    ];

}
