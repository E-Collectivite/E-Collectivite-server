<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subscription Entity.
 *
 * @property int $id
 * @property string $name
 * @property \App\Model\Entity\LocalGovernment[] $local_governments
 * @property \App\Model\Entity\Service[] $services
 */
class Subscription extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Since BelongsToMany::link() does not check for link uniqueness.
     * If we link multiple time the same service to the subscription, it will
     * appear as many times as it has been linked in the "services" array.
     * Fortunately, the link will only be created once in the database.
     *
     * This function will "clean" duplicated entries in the "services" array.
     */
    public function cleanup() {
        $this->services = collection($this->services)->indexBy('id')->toList();
    }

    /**
     * Checks of the service $service_id exists in the subscription.
     * If it exists, it is removed from the "services" array.
     *
     * @param $service_id the id of the service to remove
     * @return boolean Whether or not the service with the id $service_id exists
     * in the subscription
     */
    public function removeService($service_id) {
        $found = false;
        $this->services = collection($this->services)
            ->reject(function ($service) use ($service_id, &$found) {
                if ($service->id == $service_id) {
                    $found = ($service->id == $service_id);
                }
                return $service->id == $service_id;
            })
            ->toList();

        return $found;
    }

    public function getConnectors() {
        return collection($this->services)->extract('connector')->indexBy('id')->toList();
    }

    public function serviceExists($service) {
        return collection($this->services)->firstMatch(['name' => $service]);
    }

}
