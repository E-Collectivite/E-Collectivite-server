<?php

namespace App\Model\Entity;

use App\Network\Exception\RoleNotFoundException;
use Cake\ORM\Entity;

/**
 * UsersMembership Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $local_government_id
 * @property bool $local_government_administrator
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\LocalGovernment $local_government
 * @property \App\Model\Entity\Role[] $roles
 */
class UsersMembership extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
      '*' => true,
      'id' => false
    ];

    /**
     * Checks if the role $role_id is granted to the member for the local government.
     * If it is, remove the access to the member.
     *
     * @param $role_id the id of the role to remove
     * @throws RoleNotFoundException if the user is not member of the local government
     */
    public function removeRole($role_id) {
        $found = false;
        $this->roles = collection($this->roles)
            ->reject(function ($role) use ($role_id, &$found) {
                if (!$found) {
                    $found = ($role->id == $role_id);
                }
                return $role->id == $role_id;
            })
            ->toList();

        if(!$found) {
            throw new RoleNotFoundException("The role with the id $role_id does not exist");
        }
    }

}
