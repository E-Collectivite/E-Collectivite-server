<?php

namespace App\Model\Entity;

use App\Network\Exception\UserNotFoundException;
use Cake\ORM\Entity;

/**
 * LocalGovernment Entity
 *
 * @property int $id
 * @property string $name
 * @property string $siren
 * @property int $subscription_id
 * @property int $pooling_local_governments_id
 *
 * @property \App\Model\Entity\Subscription $subscription
 * @property \App\Model\Entity\PoolingLocalGovernment $pooling_local_government
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\UsersMembership[] $users_memberships
 */
class LocalGovernment extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Checks if the user $user_id is member of the local government.
     * If the user is, it is returned otherwise an exception is thrown.
     *
     * @param $user_id the id of the user to search
     * @throws UserNotFoundException if the user with the
     * id $user_id is not member of the local government
     * @return The user if it exists
     */
    public function getMember($user_id) {
        $member = collection($this->users)->firstMatch(['id' => $user_id]);

        if (!$member) {
            throw new UserNotFoundException("The user with the id $user_id does not exist");
        }

        $member->local_government_administrator = $member->_joinData->local_government_administrator;
        return $member;
    }

    /**
     * Checks if the user $user_id is member of the local government.
     * If so, it is removed from the local government.
     *
     * @param $user_id the id of the user to remove
     * @throws UserNotFoundException if the user is not member of the local government
     */
    public function removeMember($user_id) {
        $found = false;
        $this->users = collection($this->users)
            ->reject(function ($user) use ($user_id, &$found) {
                if (!$found) {
                    $found = ($user->id == $user_id);
                }
                return $user->id == $user_id;
            })
            ->toList();

        if (!$found) {
            throw new UserNotFoundException("The user with the id $user_id does not exist");
        }
    }

    public function isAdministrator($user_id) {
        $isAdmin = false;
        collection($this->users_memberships)->each(function ($user) use ($user_id, &$isAdmin) {
            if ($user->user_id == $user_id) {
                $isAdmin = $user->local_government_administrator;
            }
        });
        return $isAdmin;
    }

    public function isMember($user_id) {
        $isMember = false;
        collection($this->users_memberships)->each(function ($user) use ($user_id, &$isMember) {
            if ($user->user_id == $user_id) {
                $isMember = true;
            }
        });
        return $isMember;
    }

}
