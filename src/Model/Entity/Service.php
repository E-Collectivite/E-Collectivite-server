<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Service Entity
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property int $connector_id
 * @property string $class
 *
 * @property \App\Model\Entity\Connector $connector
 * @property \App\Model\Entity\Role[] $roles
 * @property \App\Model\Entity\Subscription[] $subscriptions
 */
class Service extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'name' => false
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        // Hide it from Subscriptions as it is not a useful information
        '_joinData'
    ];
}
