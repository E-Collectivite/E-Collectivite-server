<?php

namespace App\Model\Table;

use App\Network\Exception\UserNotFoundException;
use ArrayObject;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Zend\Ldap\Attribute;
use Zend\Ldap\Ldap;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $UsersMemberships
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');

//        $this->hasMany('UsersMemberships', [
//          'foreignKey' => 'user_id'
//        ]);
        $this->belongsToMany('LocalGovernments', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'local_government_id',
            'joinTable' => 'users_memberships'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create', __('The name is required'))
            ->notEmpty('name', __('The name cannot be empty'))
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('The provided user already exists')]);

        $validator
            ->requirePresence('password', 'create', __('The password is required'))
            ->notEmpty('password', __('The password cannot be empty'));

        $validator
            ->requirePresence('mail', 'create')
            ->notEmpty('mail')
            ->add('mail', 'unique', ['rule' => 'validateUnique', 'provider' => 'table'])
            ->add('mail', 'validFormat', ['rule' => 'email', 'message' => __('E-mail must be valid')]);

        $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname');

        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname');

        $validator
            ->boolean('superadmin')
            ->requirePresence('superadmin', 'create')
            ->notEmpty('superadmin');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->isUnique(['mail']));
        return $rules;
    }

    /**
     *
     * ### Usage
     *
     * Get a user:
     *
     * ```
     * $user = $users->getUser(1);
     * ```
     *
     * @throws UserNotFoundException if the user with such id could not be found
     */
    public function getUser($primary_key, $options = []) {
        try {
            $user = $this->get($primary_key, $options);
        } catch (\Exception $e) {
            throw new UserNotFoundException(__("The user with the id $primary_key does not exist"));
        }
        return $user;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        $raw_password = $entity->password;
        $entity->password = (new DefaultPasswordHasher)->hash($entity->password);

        $ldap_conf = Configure::read('Ldap');

        if ($ldap_conf) {

            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true,
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (\Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            if ($entity->isNew()) {
                $entry = [];
                Attribute::setAttribute($entry, 'cn', $entity->name);
                Attribute::setAttribute($entry, 'givenName', $entity->firstname);
                Attribute::setAttribute($entry, 'sn', $entity->lastname);
                Attribute::setAttribute($entry, 'mail', $entity->mail);
                Attribute::setPassword($entry, $raw_password, Attribute::PASSWORD_HASH_SSHA);
                Attribute::setAttribute($entry, 'objectClass', $ldap_conf['user_objectclass']);

                try {
                    $ldap->add('uid=' . $entity->name . ',' . $ldap_conf['user_base_dn'], $entry);
                } catch (\Exception $e) {
                    $entity->errors('ldap', ['name' => 'User already exists']);
                    return false;
                }
            } else {
                $entry = $ldap->getEntry('uid=' . $entity->name . ',' . $ldap_conf['user_base_dn']);
                if (is_null($entry)) {
                    $entity->errors('ldap', ['name' => 'User does not exist']);
                    return false;
                }

                Attribute::setAttribute($entry, 'cn', $entity->name);
                if ($entity->dirty('firstname')) {
                    Attribute::setAttribute($entry, 'givenName', $entity->firstname);
                }
                if ($entity->dirty('lastname')) {
                    Attribute::setAttribute($entry, 'sn', $entity->lastname);
                }
                if ($entity->dirty('mail')) {
                    Attribute::setAttribute($entry, 'mail', $entity->mail);
                }
                if ($entity->dirty('password')) {
                    Attribute::setPassword($entry, $entity->password, Attribute::PASSWORD_HASH_SSHA);
                    $entity->password = (new DefaultPasswordHasher)->hash($entity->password);
                }
                try {
                    $ldap->update('uid=' . $entity->name . ',' . $ldap_conf['user_base_dn'], $entry);
                } catch (\Exception $e) {
                    $entity->errors('ldap', ['update' => 'User does not exist']);
                    return false;
                }
            }
        }
    }

    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options) {
        $ldap_conf = Configure::read('Ldap');
        if ($ldap_conf) {


            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true,
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (\Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            try {
                $ldap->delete('uid=' . $entity->name . ',' . $ldap_conf['user_base_dn']);
            } catch (\Exception $e) {
                $entity->errors('ldap', ['name' => 'User does not exist']);
                return false;
            }
        }
    }
}
