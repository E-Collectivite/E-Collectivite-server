<?php

namespace App\Model\Table;

use App\Network\Exception\LocalGovernmentNotFoundException;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Zend\Ldap\Attribute;
use Zend\Ldap\Ldap;

/**
 * LocalGovernments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Subscriptions
 * @property \Cake\ORM\Association\BelongsTo $PoolingLocalGovernments
 * @property \Cake\ORM\Association\BelongsToMany $Users
 * @property \Cake\ORM\Association\HasMany $UsersMemberships
 *
 * @method \App\Model\Entity\LocalGovernment get($primaryKey, $options = [])
 * @method \App\Model\Entity\LocalGovernment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LocalGovernment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LocalGovernment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LocalGovernment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LocalGovernment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LocalGovernment findOrCreate($search, callable $callback = null)
 */
class LocalGovernmentsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('local_governments');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Subscriptions', [
            'foreignKey' => 'subscription_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PoolingLocalGovernments', [
            'className' => 'LocalGovernments',
            'foreignKey' => 'pooling_local_governments_id'
        ]);
        $this->hasMany('UsersMemberships', [
            'foreignKey' => 'local_government_id'
        ]);

        $this->belongsToMany('Users', [
            'foreignKey' => 'local_government_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_memberships'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('siren', 'create')
            ->notEmpty('siren')
            ->lengthBetween('siren', [9, 9], 'The siren needs to be 9 digits long')
            ->numeric('siren', 'The siren can only contain digits')
            ->add('siren', 'valid', ['rule' => 'validSiren', 'provider' => 'table']);

        $validator
            ->requirePresence('subscription_id', 'create')
            ->notEmpty('subscription_id');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['subscription_id'], 'Subscriptions'));
        $rules->add($rules->existsIn(['pooling_local_governments_id'], 'PoolingLocalGovernments'));
        return $rules;
    }

    /**
     * Take each digit of the siren one by one
     * If its index is odd, double its value
     * If the doubled value is > 9, substract 9
     * Add this value to the total sum
     * The code is valid if the total sum is a multiple of 10
     *
     * Example with the ADULLACT SIREN code:
     * SIREN code : 443783170
     * Index :      012345678
     *
     * O is even => 4*1 = 4
     * 1 is odd => 4*2 = 8
     * ...
     * 3 is odd => 7*2 = 14; 14 > 9 => 14-9 = 5
     * ...
     * 8 is even => 0*1 = 0
     *
     * Sum = 4 + 8 + 3 + 5 + 8 + 6 + 1 + 5 + 0 = 40
     * 40 mod 10 = 0
     * So the SIREN is valid
     */
    public function validSiren($value, $context) {
        if (strlen($value) != 9) {
            return false;
        }

        $sum = 0;
        for ($index = 0; $index < 9; $index++) {
            $number = (int)$value[$index];
            if (($index % 2) != 0) {
                if (($number *= 2) > 9) {
                    $number -= 9;
                }
            }
            $sum += $number;
        }

        if (($sum % 10) != 0) {
            return false;
        }

        return true;
    }

    /**
     * ### Usage
     *
     * Get a local government:
     *
     * ```
     * $localGovernment = $localGovernments->getLocalGovernment(1);
     * ```
     *
     * @throws LocalGovernmentNotFoundException if the local government with such id could not be found
     */
    public function getLocalGovernment($primary_key, $options = []) {
        try {
            $localGovernment = $this->get($primary_key, $options);
        } catch (\Exception $e) {
            throw new LocalGovernmentNotFoundException(__("The local government with the id $primary_key does not exist"));
        }
        return $localGovernment;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        $ldap_conf = Configure::read('Ldap');
        if ($ldap_conf) {
            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true,
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (\Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            if ($entity->isNew()) {
                $entry = [];
                Attribute::setAttribute($entry, 'objectClass', $ldap_conf['local_government_objectclass']);
                // LDAP class 'groupOfNames' requires a member attribute
                // FIXME: workaround using the current authenticated user, no access to "Auth", maybe use Muffin/Footprint plugin
                Attribute::setAttribute($entry, 'member', 'uid=' . $_SESSION['Auth']['User']['name'] . ',' . $ldap_conf['user_base_dn']);
//            Attribute::setAttribute($entry, 'member', 'uid=admin,' . $ldap_conf['user_base_dn']);

                try {
                    $ldap->add('cn=' . $entity->name . ',' . $ldap_conf['local_government_base_dn'], $entry);
                } catch (\Exception $e) {
                    $entity->errors('ldap', ['name' => 'Local government already exists']);
                    return false;
                }
            } else {
                //TODO
//            $entry = $ldap->getEntry('cn=' . $entity->name . ',' . $ldap_conf['local_government_base_dn']);
//            if (is_null($entry)) {
//                $entity->errors('ldap', ['name' => 'Local government does not exist']);
//                return false;
//            }
            }
        }
    }

    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options) {
        $ldap_conf = Configure::read('Ldap');
        if ($ldap_conf) {
            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true,
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (\Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            try {
                $ldap->delete('cn=' . $entity->name . ',' . $ldap_conf['local_government_base_dn']);
            } catch (\Exception $e) {
                $entity->errors('ldap', ['name' => 'Local government does not exist']);
                return false;
            }
        }
    }
}
