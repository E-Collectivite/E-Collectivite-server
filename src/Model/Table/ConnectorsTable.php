<?php
namespace App\Model\Table;

use App\Network\Exception\ConnectorNotFoundException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Connectors Model
 *
 * @property \Cake\ORM\Association\HasMany $Services
 *
 * @method \App\Model\Entity\Connector get($primaryKey, $options = [])
 * @method \App\Model\Entity\Connector newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Connector[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Connector|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Connector patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Connector[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Connector findOrCreate($search, callable $callback = null)
 */
class ConnectorsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('connectors');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Services', [
            'foreignKey' => 'connector_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url')
            ->add('url', 'validFormat', ['rule' => ['url', true], 'message' => __('URL must be valid')]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    /**
     * ### Usage
     *
     * Get a connector:
     *
     * ```
     * $connector = $connectors->getConnector(1);
     * ```
     *
     * @throws ConnectorNotFoundException if the connector with such id
     * could not be found
     */
    public function getConnector($primary_key, $options = []) {
        try {
            $connector = $this->get($primary_key, $options);
        } catch (\Exception $e) {
            throw new ConnectorNotFoundException(__("The connector with the id $primary_key does not exist"));
        }
        return $connector;
    }

}
