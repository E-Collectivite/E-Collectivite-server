<?php
namespace App\Model\Table;

use App\Network\Exception\RoleNotFoundException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersMembershipsRoles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\BelongsTo $UsersMemberships
 *
 * @method \App\Model\Entity\UsersMembershipsRole get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembershipsRole findOrCreate($search, callable $callback = null)
 */
class UsersMembershipsRolesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users_memberships_roles');
        $this->displayField('role_id');
        $this->primaryKey(['role_id', 'user_membership_id']);

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UsersMemberships', [
            'foreignKey' => 'user_membership_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['user_membership_id'], 'UsersMemberships'));

        return $rules;
    }

    /**
     * Find the role of the user if the local government, user and role exist
     *
     * @param Query $query The query finder
     * @param array $options The options passed in the query builder
     * @return Query
     * @throws RoleNotFoundException if the role with such id could not be found
     */
    public function findUserRole(Query $query, array $options) {
        $role_id = $options['role_id'];

        $query->contain([
            'UsersMemberships' => function ($q) use ($options) {
                return $q
                    ->find('localGovernment', ['local_government_id' => $options['local_government_id']])
                    ->find('user', ['user_id' => $options['user_id']]);
            }
        ])->where(['role_id' => $role_id]);

        if ($query->isEmpty()) {
            throw new RoleNotFoundException("The role with the id $role_id does not exist");
        }
        return $query;
    }

}
