<?php

namespace App\Model\Table;

use App\Network\Exception\LocalGovernmentNotFoundException;
use App\Network\Exception\UserNotFoundException;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Exception;
use Zend\Ldap\Ldap;

/**
 * UsersMemberships Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $LocalGovernments
 * @property \Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\UsersMembership get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersMembership newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersMembership[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembership|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersMembership patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembership[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersMembership findOrCreate($search, callable $callback = null)
 */
class UsersMembershipsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users_memberships');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('LocalGovernments', [
            'foreignKey' => 'local_government_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Roles', [
            'foreignKey' => 'user_membership_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'users_memberships_roles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('local_government_administrator')
            ->requirePresence('local_government_administrator', 'create')
            ->notEmpty('local_government_administrator');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['local_government_id'], 'LocalGovernments'));
        $rules->add($rules->isUnique(['local_government_id', 'user_id']));

        return $rules;
    }

    /**
     * Find the membership if the local government exists
     *
     * @param Query $query The query finder
     * @param array $options The options passed in the query builder
     * @return Query
     * @throws LocalGovernmentNotFoundException if the local government with such id could not be found
     */
    public function findLocalGovernment(Query $query, array $options) {
        $local_government_id = $options['local_government_id'];
        $query->where([
            'local_government_id' => $local_government_id
        ]);

        if ($query->isEmpty()) {
            throw new LocalGovernmentNotFoundException(__("The local government with the id $local_government_id does not exist"));
        }
        return $query;
    }

    /**
     * Find the membership if the user exists
     *
     * @param Query $query The query finder
     * @param array $options The options passed in the query builder
     * @return Query
     * @throws UserNotFoundException if the user with such id could not be found
     */
    public function findUser(Query $query, array $options) {
        $user_id = $options['user_id'];
        $query->where([
            'user_id' => $user_id
        ]);

        if ($query->isEmpty()) {
            throw new UserNotFoundException("The user with the id $user_id does not exist");
        }

        return $query;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        $ldap_conf = Configure::read('Ldap');
        if ($ldap_conf) {
            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            if ($entity->isNew()) {
                $user = $this->Users->get($entity->user_id);
                $local_government = $this->LocalGovernments->getLocalGovernment($entity->local_government_id);

                $local_government_entry = 'cn=' . $local_government->name . ',' . $ldap_conf['local_government_base_dn'];
                $entry = $ldap->getEntry($local_government_entry);
                if (is_null($entry)) {
                    $entity->errors('ldap', ['name' => 'The local government does not exist']);
                    return false;
                }

                $member['member'] = 'uid=' . $user->name . ',' . $ldap_conf['user_base_dn'];
                try {
                    $ldap->addAttributes($local_government_entry, $member);
                } catch (Exception $e) {
                    // Do nothing
                }
            }
        }
    }

    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options) {
        $ldap_conf = Configure::read('Ldap');
        if ($ldap_conf) {
            $ldap_options = [
                'host' => $ldap_conf['host'],
                'port' => $ldap_conf['port'],
                'username' => $ldap_conf['bind_dn'],
                'password' => $ldap_conf['bind_password'],
                'bindRequiresDn' => true
            ];
            $ldap = new Ldap($ldap_options);

            try {
                $ldap->bind();
            } catch (Exception $e) {
                $entity->errors('ldap', ['credentials' => 'Invalid credentials']);
                return false;
            }

            $user = $this->Users->get($entity->user_id);
            $local_government = $this->LocalGovernments->getLocalGovernment($entity->local_government_id);

            $local_government_entry = 'cn=' . $local_government->name . ',' . $ldap_conf['local_government_base_dn'];
            $entry = $ldap->getEntry($local_government_entry);
            if (is_null($entry)) {
                $entity->errors('ldap', ['name' => 'The local government does not exist']);
                return false;
            }

            $member['member'] = 'uid=' . $user->name . ',' . $ldap_conf['user_base_dn'];
            try {
                $ldap->deleteAttributes($local_government_entry, $member);
            } catch (Exception $e) {
                // Do nothing
            }
        }
    }
}
