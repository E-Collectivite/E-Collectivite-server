<?php

namespace App\Model\Table;

use App\Model\Entity\SubscriptionsService;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SubscriptionsServices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Subscriptions
 * @property \Cake\ORM\Association\BelongsTo $Services
 */
class SubscriptionsServicesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('subscriptions_services');
        $this->displayField('subscription_id');
        $this->primaryKey(['subscription_id', 'service_id']);

        $this->belongsTo('Subscriptions', [
          'foreignKey' => 'subscription_id',
          'joinType' => 'INNER'
        ]);
        $this->belongsTo('Services', [
          'foreignKey' => 'service_id',
          'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['subscription_id'], 'Subscriptions'));
        $rules->add($rules->existsIn(['service_id'], 'Services'));
        return $rules;
    }

}
