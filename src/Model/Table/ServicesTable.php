<?php
namespace App\Model\Table;

use App\Network\Exception\ServiceNotFoundException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Connectors
 * @property \Cake\ORM\Association\HasMany $Roles
 * @property \Cake\ORM\Association\BelongsToMany $Subscriptions
 *
 * @method \App\Model\Entity\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, callable $callback = null)
 */
class ServicesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('services');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Connectors', [
            'foreignKey' => 'connector_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Roles', [
            'foreignKey' => 'service_id'
        ]);
        $this->belongsToMany('Subscriptions', [
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'subscription_id',
            'joinTable' => 'subscriptions_services'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->allowEmpty('class');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['connector_id'], 'Connectors'));

        return $rules;
    }

    /**
     * ### Usage
     *
     * Get a service:
     *
     * ```
     * $service = $services->getService(1);
     * ```
     *
     * @throws ServiceNotFoundException if the service with such id
     * could not be found
     */
    public function getService($primary_key, $options = []) {
        try {
            $service = $this->get($primary_key, $options);
        } catch (\Exception $e) {
            throw new ServiceNotFoundException(__("The service with the id $primary_key does not exist"));
        }
        return $service;
    }
}
