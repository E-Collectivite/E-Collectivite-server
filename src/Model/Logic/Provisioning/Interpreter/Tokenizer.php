<?php

namespace App\Model\Logic\Provisioning\Interpreter;

/**
 * Class Tokenizer
 * Splits a string into a succession of lexemes
 */
class Tokenizer {
    protected $_input;
    protected $_currentIndex;
    protected $_currentLexemeType;
    protected $_currentLexeme;
    protected $_currentChar;

    public function __construct($input) {
        $this->_input = $input;
        $this->_currentIndex = 0;
        $this->_currentLexemeType = LexemeType::NONE;

        $this->_currentChar = '';
    }

    /**
     * Returns the current lexeme or an empty string if there are no more lexemes
     */
    public function currentLexeme() {
        if ($this->_currentLexemeType == LexemeType::NONE) {
            $this->_nextLexeme();
        }

        return $this->_currentLexeme;
    }

    /**
     * Returns the type of the current lexeme or NONE if there are no more lexemes
     */
    public function currentLexemeType() {
        if ($this->_currentLexemeType == LexemeType::NONE) {
            $this->_nextLexeme();
        }

        return $this->_currentLexemeType;
    }

    /**
     * Go to the next lexeme
     */
    public function acceptLexeme() {
        if ($this->_currentLexemeType != LexemeType::NONE) {
            ++$this->_currentIndex;
            $this->_currentLexemeType = LexemeType::NONE;
            $this->_currentChar = '';

            //After accepting a lexeme, bypass the space between the current and the next lexeme
            if ($this->_currentIndex < strlen($this->_input) && $this->_input[$this->_currentIndex] === ' ') {
                ++$this->_currentIndex;
            }
        }
    }

    private function _nextLexeme() {
        if ($this->_currentIndex > strlen($this->_input) - 1) {
            return;
        }

        $this->_currentChar = $this->_input[$this->_currentIndex];
        $this->_currentLexeme = $this->_currentChar;

        if (!in_array($this->_currentChar, LexemeType::SEPARATORS)) {
            while (preg_match('/^[a-zA-Z0-9_\-]+$/', $this->_currentChar)) {
                $nextIndex = $this->_currentIndex + 1;
                if ($nextIndex > strlen($this->_input) - 1 ||
                    (
                        in_array($this->_input[$nextIndex], LexemeType::SEPARATORS) ||
                        $this->_input[$nextIndex] === ' '
                    )
                ) {
                    break;
                }

                ++$this->_currentIndex;
                $this->_currentChar = $this->_input[$this->_currentIndex];
                $this->_currentLexeme .= $this->_currentChar;
            }
        }

        $this->_determineCurrentLexemeType();
    }

    private function _determineCurrentLexemeType() {

        if ($this->_currentChar == LexemeType::LEFT_PARENTHESIS ||
            $this->_currentChar == LexemeType::RIGHT_PARENTHESIS
        ) {
            $this->_currentLexemeType = LexemeType::SEPARATOR;
        } elseif (in_array($this->_currentLexeme, LexemeType::OPERATORS)) {
            $this->_currentLexemeType = LexemeType::OPERATOR;
        } elseif (preg_match('/^[a-zA-Z0-9_\-]+$/', $this->_currentChar)) {
            $this->_currentLexemeType = LexemeType::CONSTANT;
        } else {
            $this->_currentLexemeType = LexemeType::UNKNOWN;
        }
    }
}
