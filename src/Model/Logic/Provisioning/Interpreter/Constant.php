<?php

namespace App\Model\Logic\Provisioning\Interpreter;

use App\Model\Entity\Subscription;

class Constant implements Node {

    protected $_value;

    public function __construct($service, Subscription $subscription) {
        $this->_value = $subscription->serviceExists($service);
    }

    /**
     * @return bool the value of the node
     */
    public function value() {
        return $this->_value;
    }
}
