<?php

namespace App\Model\Logic\Provisioning\Interpreter;


class NotExpr extends Operation {

    public function evaluate($leftOperand, $rightOperand) {
        return !$leftOperand;
    }
}
