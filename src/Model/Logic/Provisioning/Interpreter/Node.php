<?php

namespace App\Model\Logic\Provisioning\Interpreter;

interface Node {

    /**
     * @return the value of the node
     */
    public function value();
}
