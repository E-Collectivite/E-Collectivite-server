<?php

namespace App\Model\Logic\Provisioning\Interpreter;

/**
 * Class Parser
 * Recursive descent parser
 * Parse a string and evaluate a boolean expression
 *
 * We refer to a boolean expression with this EBNF grammar :
 *
 * expression ::= <term> [ 'OR' <term> ]
 * term ::= <factor> [ 'AND' <factor> ]
 * factor ::= <constant> | 'NOT' <factor> | '(' <expression> ')'
 * constant ::= true | false
 */
class Parser {
    protected $_tokenizer;
    protected $_subscription;

    public function __construct($subscription) {
        $this->_subscription = $subscription;
    }

    public function parse($input) {
        $this->_tokenizer = new Tokenizer($input);
        return $this->_expression()->value();
    }


    private function _expression() {
        $expression = $this->_term();

        while ($this->_tokenizer->currentLexemeType() != LexemeType::NONE && $this->_tokenizer->currentLexeme() == LexemeType::OR) {
            $leftOperand = $expression;
            $this->_tokenizer->acceptLexeme();
            $rightOperand = $this->_term();
            $expression = new OrExpr($leftOperand, $rightOperand);
        }

        return $expression;
    }

    private function _term() {
        $term = $this->_factor();

        while ($this->_tokenizer->currentLexemeType() != LexemeType::NONE && $this->_tokenizer->currentLexeme() == LexemeType::AND) {
            $leftOperand = $term;
            $this->_tokenizer->acceptLexeme();
            $rightOperand = $this->_factor();
            $term = new AndExpr($leftOperand, $rightOperand);
        }

        return $term;
    }

    private function _factor() {
        $factor = null;

        $currentLexeme = $this->_tokenizer->currentLexeme();
        $currentLexemeType = $this->_tokenizer->currentLexemeType();

        if ($currentLexemeType == LexemeType::CONSTANT) {
            $factor = new Constant($currentLexeme, $this->_subscription);
            $this->_tokenizer->acceptLexeme();
        } elseif ($currentLexeme == LexemeType::NOT) {
            $this->_tokenizer->acceptLexeme();
            $factor = $this->_factor();
            //TODO: have only one parameter for the Not operation
            $operation = new NotExpr($factor, $factor);
            $factor = $operation;
        } elseif ($currentLexeme == LexemeType::LEFT_PARENTHESIS) {
            $this->_tokenizer->acceptLexeme();
            $factor = $this->_expression();
            if ($this->_tokenizer->currentLexeme() == LexemeType::RIGHT_PARENTHESIS) {
                $this->_tokenizer->acceptLexeme();
            }
        }

        return $factor;
    }

}
