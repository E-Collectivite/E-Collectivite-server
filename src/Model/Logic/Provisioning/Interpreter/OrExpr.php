<?php

namespace App\Model\Logic\Provisioning\Interpreter;

class OrExpr extends Operation {

    public function evaluate($leftOperand, $rightOperand) {
        return $leftOperand || $rightOperand;
    }
}
