<?php

namespace App\Model\Logic\Provisioning\Interpreter;

class AndExpr extends Operation {

    public function evaluate($leftOperand, $rightOperand) {
        return $leftOperand && $rightOperand;
    }
}
