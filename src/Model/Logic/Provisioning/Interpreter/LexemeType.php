<?php

namespace App\Model\Logic\Provisioning\Interpreter;

class LexemeType {
    const OPERATORS = ['AND', 'OR', 'NOT'];
    const SEPARATORS = ['(', ')'];
    const LEFT_PARENTHESIS = '(';
    const RIGHT_PARENTHESIS = ')';
    const AND = 'AND';
    const OR = 'OR';
    const NOT = 'NOT';

    const NONE = 1; // empty Lexeme
    const CONSTANT = 2;
    const OPERATOR = 3;
    const SEPARATOR = 4;
    const UNKNOWN = 5;

}
