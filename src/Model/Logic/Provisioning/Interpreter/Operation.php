<?php

namespace App\Model\Logic\Provisioning\Interpreter;

abstract class Operation implements Node {
    protected $_leftOperand;
    protected $_rightOperand;

    public function __construct(Node $leftOperand, Node $rightOperand) {
        $this->_leftOperand = $leftOperand;
        $this->_rightOperand = $rightOperand;
    }

    /**
     * @return the value of the node
     */
    public function value() {
        $leftValue = $this->_leftOperand->value();
        $rightValue = $this->_rightOperand->value();

        return $this->evaluate($leftValue, $rightValue);
    }

    abstract public function evaluate($leftOperand, $rightOperand);
}
