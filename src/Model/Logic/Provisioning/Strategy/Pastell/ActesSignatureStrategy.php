<?php

namespace App\Model\Logic\Provisioning\Strategy\Pastell;

use App\Model\Logic\Provisioning\Strategy\PastellStrategy;

class ActesSignatureStrategy extends PastellStrategy {

    public function execute($http, $url, $entity, $subscription) {
        $response = $http->createConnector($this->_pastellEntity['id_e'], 'iParapheur', 'iParapheur - Actes');
        $configConnector = $this->_configuration['iParapheur'];
        $configConnector['iparapheur_user_certificat'] = fopen($configConnector['iparapheur_user_certificat'], 'r');
        $http->configureConnector($this->_pastellEntity['id_e'], $response['id_ce'], $configConnector);
        $http->associateFluxWithConnector($this->_pastellEntity['id_e'], $response['id_ce'], 'actes-generique', 'signature');
    }
}
