<?php

namespace App\Model\Logic\Provisioning\Strategy\Pastell;

use App\Model\Logic\Provisioning\Strategy\PastellStrategy;

class TdtActesStrategy extends PastellStrategy {

    public function execute($http, $url, $entity, $subscription) {
        $response = $http->createConnector($this->_pastellEntity['id_e'], 's2low', 's2low - Actes');
        $configConnector = $this->_configuration['s2low'];
        $configConnector['server_certificate'] = fopen($configConnector['server_certificate'], 'rb');
        $configConnector['user_certificat'] = fopen($configConnector['user_certificat'], 'rb');
        $http->configureConnector($this->_pastellEntity['id_e'], $response['id_ce'], $configConnector);
        $http->associateFluxWithConnector($this->_pastellEntity['id_e'], $response['id_ce'], 'actes-generique', 'TdT');

        $http->triggerActionOnConnector($this->_pastellEntity['id_e'], 'TdT', 'actes-generique', 'demande-classification');
        $http->triggerActionOnConnector($this->_pastellEntity['id_e'], 'TdT', 'actes-generique', 'recup-classification');
    }
}
