<?php

namespace App\Model\Logic\Provisioning\Strategy\Pastell;

use App\Model\Logic\Provisioning\Strategy\PastellStrategy;

class MailsecStrategy extends PastellStrategy {

    public function execute($http, $url, $entity, $subscription) {
        $response = $http->createConnector($this->_pastellEntity['id_e'], 'mailsec', 'Mail sécurisé');
        $http->associateFluxWithConnector($this->_pastellEntity['id_e'], $response['id_ce'], 'mailsec', 'mailsec');
    }

}
