<?php

namespace App\Model\Logic\Provisioning\Strategy\Parapheur;

use App\Model\Logic\Provisioning\Strategy\ParapheurStrategy;

class SignatureHeliosStrategy extends ParapheurStrategy {

    public function execute($http, $url, $entity, $subscription) {
        $officeName = $entity->name . '-Helios';
        $officeId = $http->createOffice($officeName, $officeName, $officeName);
        $subOfficeName = $officeName . '-Pastell';
        $subOfficeId = $http->createSubOffice($officeId['id'], $subOfficeName, $subOfficeName, $subOfficeName);

        $login = "pastell-helios@" . $entity->name;
        $user = $http->createUser($login, $login, $login, $login);

        $http->addOwnersToOffice($subOfficeId['id'], [$login]);
        $subtype = $http->getSubtype('PES', 'HELIOS-Signature');
        $http->addOfficeToSubtype($subtype, $subOfficeId['id']);
    }
}
