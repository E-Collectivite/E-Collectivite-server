<?php

namespace App\Model\Logic\Provisioning\Strategy\Parapheur;

use App\Model\Logic\Provisioning\Strategy\ParapheurStrategy;

class SignatureActesStrategy extends ParapheurStrategy {

    public function execute($http, $url, $entity, $subscription) {
        $officeId = $http->createOffice($entity->name . '-Actes', $entity->name . '-Actes', $entity->name . '-Actes');
        $subOfficeId = $http->createSubOffice($officeId['id'], $entity->name . '-Actes-Pastell', $entity->name . '-Actes-Pastell', $entity->name . '-Actes-Pastell');

        $login = "pastell-actes@" . $entity->name;
        $user = $http->createUser($login, $login, $login, $login);
        
        $http->addOwnersToOffice($subOfficeId['id'], [$login]);
        $subtype = $http->getSubtype('ACTES', 'ACTES-Signature');
        $http->addOfficeToSubtype($subtype, $subOfficeId['id']);
    }
}
