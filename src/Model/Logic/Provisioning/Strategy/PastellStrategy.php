<?php

namespace App\Model\Logic\Provisioning\Strategy;

abstract class PastellStrategy implements ProvisioningStrategy {
    protected $_pastellEntity;
    protected $_pastellEntityConnectors;
    protected $_configuration;

    public function getPastellEntity() {
        return $this->_pastellEntity;
    }

    public function setPastellEntity($pastellEntity) {
        $this->_pastellEntity = $pastellEntity;
    }

    public function getPastellEntityConnectors() {
        return $this->_pastellEntityConnectors;
    }

    public function setPastellConnectors($pastellEntityConnectors) {
        $this->_pastellConnectors = $pastellEntityConnectors;
    }

    public function setConfiguration($configuration) {
        $this->_configuration = $configuration;
    }

}
