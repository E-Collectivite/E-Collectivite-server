<?php

namespace App\Model\Logic\Provisioning\Strategy;

interface ProvisioningStrategy {
    public function execute($http, $url, $entity, $subscription);
}
