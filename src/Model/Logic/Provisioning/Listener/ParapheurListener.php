<?php

namespace App\Model\Logic\Provisioning\Listener;

use App\Model\Entity\LocalGovernment;
use App\Model\Entity\User;
use App\Model\Entity\UsersMembershipsRole;
use App\Model\Logic\Http\Client\Parapheur;
use App\Model\Logic\Provisioning\Interpreter\Parser;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;

class ParapheurListener implements EventListenerInterface {
    const CONNECTOR = 'parapheur';
    protected $_configuration;
    protected $_http;

    public function __construct() {
        $this->_configuration = Configure::read(self::CONNECTOR);
        $client = new Client(['ssl_verify_peer' => false]);
        $connectors = TableRegistry::get('Connectors');
        $parapheur = $connectors->findByName(self::CONNECTOR)->first();
        $this->_http = new Parapheur($client, $parapheur->url);
        $this->_http->setCredentials($this->_configuration['auth']['username'], $this->_configuration['auth']['password']);
    }


    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * ### Example:
     *
     * ```
     *  public function implementedEvents()
     *  {
     *      return [
     *          'Order.complete' => 'sendEmail',
     *          'Article.afterBuy' => 'decrementInventory',
     *          'User.onRegister' => ['callable' => 'logRegistration', 'priority' => 20, 'passParams' => true]
     *      ];
     *  }
     * ```
     *
     * @return array associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents() {
        return [
            'Model.afterSave' => 'afterSave',
            'Model.afterDelete' => 'afterDelete'
        ];
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if ($entity instanceof User) {
            $this->_handleUser($entity);
        } elseif ($entity instanceof LocalGovernment) {
            $this->_handleLocalGovernement($entity);
        } elseif ($entity instanceof UsersMembershipsRole) {
            $this->_handleUserRoles($entity);
        }
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options) {
        if ($entity instanceof User) {
            $this->_http->deleteUser($entity->name);
        } elseif ($entity instanceof UsersMembershipsRole) {
            $this->_deleteRole($entity);
        }
    }

    private function _handleUser(User $entity) {
        $connectors = TableRegistry::get('Connectors');
        $connector = $connectors->findByName(self::CONNECTOR)->first();
        if ($connector) {
            if ($entity->isNew()) {
                $this->_http->createUser($entity->name, $entity->mail, $entity->firstname, $entity->lastname);
            } else {
                $this->_http->updateUser($entity->name, $entity->mail, $entity->firstname, $entity->lastname);
            }
        }
    }

    private function _handleLocalGovernement(LocalGovernment $entity) {
        $subscription = $this->_getSubscription($entity);

        // Get Connectors related to the subscription
        $connectors = $subscription->getConnectors();

        $connector = collection($connectors)->firstMatch(['name' => self::CONNECTOR]);
        if ($connector) {
            $parser = new Parser($subscription);
            foreach (Configure::read('Strategies.' . self::CONNECTOR) as $rule) {

                if ($parser->parse($rule['expression'])) {
                    $strategy = $rule['strategy'];
                    $strategy->execute($this->_http, $connector->url, $entity, $subscription);
                }
            }
        }

    }

    private function _getSubscription($entity) {
        $subscriptions = TableRegistry::get('Subscriptions');
        return $subscriptions->get($entity->subscription_id, ['contain' => ['Services' => ['Connectors']]]);
    }

    private function _getMembership($entity) {
        $memberships = TableRegistry::get('UsersMemberships');

        return $memberships
            ->find()
            ->where(['UsersMemberships.id' => $entity->user_membership_id])
            ->contain([
                'Roles' => [
                    'strategy' => 'subquery',
                    'queryBuilder' => function ($q) use ($entity) {
                        return $q
                            ->where(['Roles.id' => $entity->role_id]);
                    },
                    'Services' => ['Connectors']
                ],
                'LocalGovernments',
                'Users'
            ])->first();
    }

    private function _handleUserRoles(UsersMembershipsRole $entity) {
        $membership = $this->_getMembership($entity);
        $localGovernmentName = $membership->local_government->name;
        $role = $membership->roles[0];

        if ($role->service->connector->name === self::CONNECTOR) {
            $officeSuffix = Configure::read('Roles.' . self::CONNECTOR . '.' . $role->name);
            $officeName = $localGovernmentName . '-' . $officeSuffix;
            $office = $this->_http->getOfficeByName($officeName);
            $this->_http->addOwnersToOffice($office['id'], [$membership->user->name]);
        }
    }

    private function _deleteRole(UsersMembershipsRole $entity) {
        $memberships = TableRegistry::get('UsersMemberships');

        $membership = $memberships->get($entity->user_membership_id, [
            'contain' => ['LocalGovernments', 'Users']
        ]);
        $roles = TableRegistry::get('Roles');
        $role = $roles->get($entity->role_id, ['contain' => ['Services.Connectors']]);

        $officeSuffix = Configure::read('Roles.' . self::CONNECTOR . '.' . $role->name);
        $officeName = $membership->local_government->name . '-' . $officeSuffix;

        $office = $this->_http->getOfficeByName($officeName);
        $this->_http->removeOwnersFromOffice($office['id'], [$membership->user->name]);
    }
}
