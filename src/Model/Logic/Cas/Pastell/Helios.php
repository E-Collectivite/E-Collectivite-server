<?php

namespace App\Model\Logic\Cas\Pastell;

use App\Model\Logic\Cas\Pastell;
use Cake\Core\Configure;

class Helios extends Pastell {

    public function getType() {
        return 'helios-generique';
    }

    public function getFilters() {
        return Configure::read('Filters.pastell.helios');
    }
}
