<?php

namespace App\Model\Logic\Cas\Pastell;

use App\Model\Logic\Cas\Pastell;
use Cake\Core\Configure;

class Mailsec extends Pastell {

    public function getType() {
        return 'mailsec';
    }

    public function getFilters() {
        return Configure::read('Filters.pastell.mailsec');
    }
}
