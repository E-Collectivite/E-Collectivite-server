<?php

namespace App\Model\Logic\Cas\Pastell;

use App\Model\Logic\Cas\Pastell;
use Cake\Core\Configure;

class Actes extends Pastell {

    public function getType() {
        return 'actes-generique';
    }

    public function getFilters() {
        return Configure::read('Filters.pastell.actes');
    }
}
