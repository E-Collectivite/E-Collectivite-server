<?php

namespace App\Model\Logic\Cas;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\InternalErrorException;
use phpCAS;

class CasRequestHandler {

    private $url = '';
    private $body = [];
    private $service = null;

    private $responseCode = 0;
    private $responseBody = '';

    private $header = null;

    public function __construct($url, array $body = []) {
        $this->url = $url;
        $this->body = $body;

        //Take care of GET requests only for the moment
        if ($body) {
            $this->url .= '?' . http_build_query($body);
        }
    }

    public function getResponseCode() {
        return $this->responseCode;
    }

    public function getResponseBody() {
        return $this->responseBody;
    }

    public function setHeader($header = null) {
        $this->header = $header;
    }

    public function request() {
        //TODO: improve
        if ($this->header) {
            $httpObject = new Client(['ssl_verify_peer' => false]);

            //TODO: improve
            $url = 'https://' . Configure::read('Cas.hostname') . '/cas/login?service=' . urlencode($this->url);

            $resp = $httpObject->get($url . '&token=' . $this->header);

            $resp = $httpObject->get($resp->getHeader('Location')[0]);

            $resp = $httpObject->get($resp->getHeader('Location')[0]);

            $this->responseCode = $resp->getStatusCode();
            $this->responseBody = $resp->getBody();
        } else {
            if (!phpCAS::isAuthenticated()) {
                throw new ForbiddenException('You need to be authenticated to the CAS server.');
            }
            $this->service = phpCAS::getProxiedService(PHPCAS_PROXIED_SERVICE_HTTP_GET);
            $this->service->setUrl($this->url);

            try {
                $this->service->send();
                $this->responseCode = $this->service->getResponseStatusCode();
                $this->responseBody = $this->service->getResponseBody();
            } catch (\CAS_ProxyTicketException $e) {
                if ($e->getCode() === PHPCAS_SERVICE_PT_FAILURE) {
                    throw new ForbiddenException('Your login has timed out. You need to log in again.');
                } else {
                    // Other proxy ticket errors are from bad request format (shouldn't happen)
                    // or CAS server failure (unlikely) so lets just stop if we hit those.
                    throw new InternalErrorException('An error occurred while requesting the service.');
                }
            } catch (\CAS_ProxiedService_Exception $e) {
                // Something prevented the service request from being sent or received.
                // We didn't even get a valid error response (404, 500, etc), so this
                // might be caused by a network error or a DNS resolution failure.
                // We could handle it in some way, but for now we will just stop.
                throw new InternalErrorException('An error occurred while requesting the service.');
            }
        }
    }

}
