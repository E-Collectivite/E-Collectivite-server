<?php

namespace App\Model\Logic\Cas;

abstract class Service {
    protected $url = '';
    protected $local_government = '';
    protected $header = null;

    abstract public function getData();

    public function __construct($local_government, $url, $header = null) {
        $this->local_government = $local_government;
        $this->url = $url;
        $this->header = $header;
    }

}
