<?php

namespace App\Model\Logic\Cas;

use Cake\Network\Exception\InternalErrorException;

abstract class Pastell extends Service {
    private $entities = [];

    abstract public function getType();
    // Hardcode filters for the moment
    // TODO : write filters in an external config file ?
    abstract public function getFilters();

    public function getData() {
        $response = [];
        $this->__populateEntities();

        $entity = $this->__getEntity();
        if ($entity) {
            $documents = $this->__getDocuments($entity);

            foreach ($this->getFilters() as $filter => $labels) {
                $numberOfDocuments = $this->__getNumberOfDocuments($documents, $filter);
                $entry['label'] = __n($labels['singular'], $labels['plural'], $numberOfDocuments, $numberOfDocuments);
                $entry['link'] = $this->url . '/document/list.php?type=' . $this->getType() . '&id_e=' . $entity['id_e'] . '&filtre=' . $filter;
                $response[] = $entry;
            }
        }
        return $response;
    }

    /**
     * Get Pastell entities the authenticated user belongs too.
     */
    private function __populateEntities() {
        $request = new CasRequestHandler($this->url . '/api/list-entite.php?auth=cas');
        $request->setHeader($this->header);
        $request->request();
        $this->__handleErrors($request);
        $this->entities = json_decode($request->getResponseBody(), true);
    }

    /**
     * Get Pastell documents related to the current local government.
     */
    private function __getDocuments($entity) {
        $documents = [];

        $parameters = [
            'auth' => 'cas',
            'type' => $this->getType(),
            'id_e' => $entity['id_e']
        ];
        $request = new CasRequestHandler($this->url . '/api/list-document.php', $parameters);
        $request->setHeader($this->header);
        $request->request();
        $documents = json_decode($request->getResponseBody(), true);

        $this->__handleErrors($request);
        return $documents;
    }

    private function __getEntity() {
        return collection($this->entities)->firstMatch(['denomination' => $this->local_government]);
    }

    private function __getNumberOfDocuments($documents, $filter) {
        $number = 0;

        foreach ($documents as $document) {
            if ($document['last_action'] === $filter) {
                ++$number;
            }
        }
        return $number;
    }

    private function __handleErrors($request) {
        $responseBody = json_decode($request->getResponseBody(), true);

        $incorrectResponseCode = $request->getResponseCode() !== 200;
        $hasPastellStatusKey = array_key_exists('status', $responseBody);
        if ($incorrectResponseCode || $hasPastellStatusKey) {
            throw new InternalErrorException('An error occurred while requesting the service.');
        }

    }

}
