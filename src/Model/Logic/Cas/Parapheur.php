<?php

namespace App\Model\Logic\Cas;

use Cake\Core\Configure;

class Parapheur extends Service {

    public function getData() {
        $response = [];
        $request = new CasRequestHandler($this->url . '/alfresco/wcs/parapheur/bureaux');
        $request->request();

        if ($request->getResponseCode() === 200) {
            $responseBody = json_decode($request->getResponseBody(), true);

            if (empty($responseBody)) {
                $response['message'] = 'No office is set for this user';
            } else {
                foreach ($responseBody as $office) {
                    $entry = [];

                    foreach(Configure::read('Filters.parapheur') as $status => $labels) {
                        $count = (isset($office[$status])) ? (int)$office[$status] : 0;
                        $entry[] = __n($labels['singular'], $labels['plural'], $count, $count);
                    }
                    $response[$office['name']] = $entry;
                }
            }
        }
        return $response;
    }

}
