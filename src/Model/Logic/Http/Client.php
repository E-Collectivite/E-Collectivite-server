<?php

namespace App\Model\Logic\Http;

abstract class Client {
    protected $http;
    protected $base_url;
    protected $base_api;
    protected $username;
    protected $password;

    public function __construct($http, $url, $api_base = '/') {
        $this->http = $http;
        $this->base_url = $url;
        $this->base_api = $api_base;
    }

    public function setCredentials($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    private function execute($endpoint, $httpMetod, $urlParams, $bodyParams = null) {
        $url = $this->base_url . '/' . $this->base_api . '/' . $endpoint;

        if (!empty($urlParams)) {
            $url .= (strpos($url, '?') === false) ? '?' : '&';
            $url .= is_string($urlParams) ? $urlParams : http_build_query($urlParams);
        }

        if ($httpMetod === 'GET') {
            $result = $this->http->get($url);
        } elseif ($httpMetod === 'POST') {
            $result = $this->http->post($url, $bodyParams, ['type' => 'json']);
        } elseif ($httpMetod === 'PUT') {
            $result = $this->http->put($url, $bodyParams, ['type' => 'json']);
        } else {
            $result = $this->http->delete($url);
        }

        return $result;
    }

    protected function get($endpoint, $urlParams) {
        return $this->execute($endpoint, 'GET', $urlParams);
    }

    protected function post($endpoint, $urlParams, $bodyParams) {
        return $this->execute($endpoint, 'POST', $urlParams, $bodyParams);
    }

    protected function put($endpoint, $urlParams, $bodyParams) {
        return $this->execute($endpoint, 'PUT', $urlParams, $bodyParams);
    }

    protected function delete($endpoint, $urlParams) {
        return $this->execute($endpoint, 'DELETE', $urlParams);
    }


}
