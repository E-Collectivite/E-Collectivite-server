<?php

namespace App\Model\Logic\Http\Client;

use App\Model\Logic\Http\Client;
use Cake\Utility\Security;

class Pastell extends Client {

    public function __construct($http, $url, $api_base = '/api') {
        parent::__construct($http, $url, $api_base);
    }

    /**
     * @return mixed the list of all the entities
     */
    public function getEntities() {
        $endpoint = '/list-entite.php';

        $result = $this->get($endpoint, []);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * @param string $name the name of the entity
     * @return mixed the entry of the entity if it exists, otherwise null
     */
    public function getEntityByName($name) {
        $entities = $this->getEntities();
        if ($entities) {
            return collection($entities)->firstMatch(['denomination' => $name]);
        }
    }

    /**
     * @param string $id_e the id of the entity
     * @return mixed the list of pastell connectors associated with the entity $id_e
     */
    public function getConnectorsOfEntity($id_e) {
        $endpoint = '/list-connecteur-entite.php';

        $result = $this->get($endpoint, ['id_e' => $id_e]);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Creates a new entity of the entity with the name $name doesn't exist
     * @param string $name
     * @param string $siren
     * @param string $type
     * @return mixed the entry of the entity if it exists otherwise the id of the newly created entity
     */
    public function createEntity($name, $siren, $type = 'collectivite') {
        $endpoint = '/create-entite.php';

        $entity = $this->getEntityByName($name);
        if ($entity) {
            return $entity;
        }

        $data = [
            'denomination' => utf8_decode($name),
            'siren' => $siren,
            'type' => $type
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Updates the siren of the entity $name
     * @param string $name
     * @param string $siren
     * @param string $type
     * @return mixed
     */
    public function updateEntity($name, $siren, $type = 'collectivite') {
        $endpoint = '/modif-entite.php';

        $data = [
            'denomination' => utf8_decode($name),
            'siren' => $siren,
            'type' => $type
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * @return mixed the list of all users
     */
    public function getUsers() {
        $endpoint = '/list-utilisateur.php';

        $result = $this->get($endpoint, []);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * @param string $username the username of the user
     * @return mixed the entry of the user if it exists, otherwise null
     */
    public function getUserByName($username) {
        $users = $this->getUsers();
        if ($users) {
            return collection($users)->firstMatch(['login' => $username]);
        }
    }

    /**
     * Creates a new user if the user with the login $username doesn't exist
     * @param string $username
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $password
     * @return mixed the entry of the user if it exists, otherwise the id of the newly created user
     */
    public function createUser($username, $email, $firstname, $lastname, $password = null) {
        $endpoint = '/create-utilisateur.php';

        $user = $this->getUserByName($username);
        if ($user) {
            return $user;
        }
        if (empty($password)) {
            $password = Security::hash(Security::randomBytes(10));
        }

        $data = [
            'login' => utf8_decode($username),
            'prenom' => utf8_decode($firstname),
            'nom' => utf8_decode($lastname),
            'email' => $email,
            'password' => $password
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Updates the mail, firstname and lastname of the user with the login $username
     * @param string $username
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $password
     * @return mixed
     */
    public function updateUser($username, $email, $firstname, $lastname, $password = null) {
        $endpoint = '/modif-utilisateur.php';

        $user = $this->getUserByName($username);
        if ($user) {
            $data = [
                'login' => utf8_decode($username),
                'prenom' => utf8_decode($firstname),
                'nom' => utf8_decode($lastname),
                'email' => $email,
                'password' => $password
            ];

            $result = $this->post($endpoint, [], $data);
            if ($result->isOk()) {
                return $result->json;
            }
        }
    }

    /**
     * Deletes a user by its username
     * @param string $name
     * @return mixed
     */
    public function deleteUser($name) {
        $endpoint = '/delete-utilisateur.php';

        $result = $this->post($endpoint, [], ['login' => utf8_decode($name)]);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Adds the role $role to the user $username for the entity $entity
     * @param string $username
     * @param string $entity
     * @param string $role
     * @return mixed
     */
    public function addRoleToUser($username, $entity, $role) {
        $endpoint = '/add-several-roles-utilisateur.php';

        $data = [
            'login' => utf8_decode($username),
            'denomination' => utf8_decode($entity),
            'role' => $role
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Deletes the role $role from the user $username for the entity $entity
     * @param string $username
     * @param string $entity
     * @param string $role
     * @return mixed
     */
    public function deleteRoleFromUser($username, $entity, $role) {
        $endpoint = '/delete-several-roles-utilisateur.php';

        $data = [
            'login' => utf8_decode($username),
            'denomination' => utf8_decode($entity),
            'role' => $role
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Creates the connector with the id $id_entity to the entity $id_entity
     * @param string $id_entity the id of the entity
     * @param string $connectorName the name (id) of the connector
     * @param string $connectorLabel the label of the connector
     * @return mixed the id of the newly created connector
     */
    public function createConnector($id_entity, $connectorName, $connectorLabel) {
        $endpoint = '/create-connecteur-entite.php';

        $data = [
            'id_e' => $id_entity,
            'id_connecteur' => $connectorName,
            'libelle' => utf8_decode($connectorLabel)
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Configures the connector $id_connector for the entity $id_entity with the data available in $parameters
     * @param string $id_entity
     * @param string $id_connector
     * @param array $parameters
     * @return mixed
     */
    public function configureConnector($id_entity, $id_connector, $parameters = []) {
        $endpoint = '/edit-connecteur-entite.php';

        $data = [
                'id_e' => $id_entity,
                'id_ce' => $id_connector
            ] + $parameters;

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Associates the connector of an entity to a flux
     * @param string $id_entity
     * @param string $id_connector
     * @param string $flux the name of the flux
     * @param string $type the type of the connector
     * @return mixed
     */
    public function associateFluxWithConnector($id_entity, $id_connector, $flux, $type) {
        $endpoint = '/create-flux-connecteur.php';

        $data = [
            'id_e' => $id_entity,
            'id_ce' => $id_connector,
            'flux' => $flux,
            'type' => $type
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Triggers an action on a connector
     *
     * @param string $id_entity
     * @param string $type the type of the connector
     * @param string $flux the name of the flux
     * @param string $action the name of the action
     * @return mixed
     */
    public function triggerActionOnConnector($id_entity, $type, $flux, $action) {
        $endpoint = '/action-connecteur-entite.php';

        $data = [
            'id_e' => $id_entity,
            'type' => $type,
            'flux' => $flux,
            'action' => $action
        ];

        $result = $this->post($endpoint, [], $data);
        if ($result->isOk()) {
            return $result->json;
        }
    }

}
