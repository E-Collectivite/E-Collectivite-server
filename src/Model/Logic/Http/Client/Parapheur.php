<?php

namespace App\Model\Logic\Http\Client;

use App\Model\Logic\Http\Client;
use Cake\Utility\Security;

class Parapheur extends Client {
    private $accessTicket;

    public function __construct($http, $url, $api_base = '/alfresco/wcsip') {
        parent::__construct($http, $url, $api_base);
    }

    public function getAccessTicket() {
        if (empty($this->accessTicket)) {
            $this->requestAccessTicket();
        }
        return $this->accessTicket;
    }

    /**
     * Get an access ticket for the user with the given credentials
     * The ticket will be used as an url parameter for each authenticated request
     */
    private function requestAccessTicket() {
        $endpoint = '/api/login';

        $data = [
            'username' => $this->username,
            'password' => $this->password
        ];

        $result = $this->post($endpoint, [], json_encode($data));
        if ($result->isOk()) {
            $this->accessTicket = $result->json['data']['ticket'];
        }
    }

    /**
     * @return mixed the list of all the offices
     */
    public function getOffices() {
        $endpoint = '/parapheur/bureaux';

        $urlParams = [
            'ticket' => $this->getAccessTicket(),
            'asAdmin' => true
        ];

        $result = $this->get($endpoint, $urlParams);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * @param string $officeId the id of the office
     * @return mixed the entry of the office if it exists, otherwise null
     */
    public function getOffice($officeId) {
        $offices = $this->getOffices();
        if ($offices) {
            return collection($offices)->firstMatch(['id' => $officeId]);
        }
    }

    /**
     * @param string $officeName the name of the office
     * @return mixed the entry of the office if it exists, otherwise null
     */
    public function getOfficeByName($officeName) {
        $offices = $this->getOffices();
        if ($offices) {
            return collection($offices)->firstMatch(['name' => $officeName]);
        }
    }

    /**
     * Creates a new office if the office with the name $name doesn't exist
     * @param string $name
     * @param string $title
     * @param string $description
     * @return mixed the entry of the office if it exists, otherwise the id of the newly created office
     */
    public function createOffice($name, $title, $description = '') {
        $endpoint = '/parapheur/bureaux';

        $office = $this->getOfficeByName($name);
        if ($office) {
            return $office;
        }

        $data = [
            'name' => $name,
            'title' => $title,
            'description' => $description,
            'hab_traiter' => true
        ];

        $result = $this->post($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));

        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Creates a sub office for the office with the id $parentOfficeId
     * @param string $parentOfficeId
     * @param string $name
     * @param string $title
     * @param string $description
     * @return mixed the entry of the office if it exists, otherwise the id of the newly created office
     */
    public function createSubOffice($parentOfficeId, $name, $title, $description = '') {
        $endpoint = '/parapheur/bureaux';

        $office = $this->getOfficeByName($name);
        if ($office) {
            return $office;
        }
        $parentOffice = $this->getOffice($parentOfficeId);
        if (!$parentOffice) {
            return;
        }

        $data = [
            'hierarchie' => $parentOfficeId,
            'name' => $name,
            'title' => $title,
            'description' => $description,
            'hab_transmettre' => true
        ];

        $result = $this->post($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Make people in $owners owner of the office $officeId
     * @param string $officeId the id of the office
     * @param array $owners an array of username
     */
    public function addOwnersToOffice($officeId, array $owners) {
        $office = $this->getOffice($officeId);
        if ($office) {
            foreach ($owners as $username) {
                if ($this->getUserByUsername($username)
                    && !collection($office['proprietaires'])->firstMatch(['username' => $username])
                ) {
                    $office['proprietaires'][] = ['username' => $username];
                }
            }
            $data = ['proprietaires' => $office['proprietaires']];
            $endpoint = '/parapheur/bureaux/' . $officeId;
            $result = $this->put($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));

            if ($result->isOk()) {
                return $result->json;
            }
        }
    }

    /**
     * @return mixed the list of all the users
     */
    public function getUsers() {
        $endpoint = '/parapheur/utilisateurs';

        $urlParams = [
            'ticket' => $this->getAccessTicket(),
        ];

        $result = $this->get($endpoint, $urlParams);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * @param string $username the username of the user
     * @return mixed the entry of the user if it exists, otherwise null
     */
    public function getUserByUsername($username) {
        $users = $this->getUsers();
        if ($users) {
            return collection($users)->firstMatch(['username' => $username]);
        }
    }

    /**
     * Creates a new user if the user with the login $username doesn't exist
     * @param string $username
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $password
     * @return mixed the entry of the user if it exists, otherwise the id of the newly created user
     */
    public function createUser($username, $email, $firstname, $lastname, $password = null) {
        $endpoint = '/parapheur/utilisateurs';

        $user = $this->getUserByUsername($username);
        if ($user) {
            return $user;
        }
        if (empty($password)) {
            $password = Security::hash(Security::randomBytes(10));
        }

        $data = [
            'username' => $username,
            'firstName' => $firstname,
            'lastName' => $lastname,
            'email' => $email,
            'password' => $password
        ];

        $result = $this->post($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Updates the mail, firstname and lastname of the user with the login $username
     * @param string $username
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @return mixed
     */
    public function updateUser($username, $email, $firstname, $lastname) {
        $endpoint = '/parapheur/utilisateurs';

        $user = $this->getUserByUsername($username);
        if ($user) {
            $endpoint .= '/' . $user['id'];

            $data = [
                'firstName' => $firstname,
                'lastName' => $lastname,
                'email' => $email
            ];

            $result = $this->put($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));

            if ($result->isOk()) {
                return $result->json;
            }
        }
    }

    /**
     * Deletes a user by its username
     * @param string $username
     * @return mixed
     */
    public function deleteUser($username) {
        $endpoint = '/parapheur/utilisateurs';

        $user = $this->getUserByUsername($username);
        if ($user) {
            $endpoint .= '/' . $user['id'];

            $result = $this->delete($endpoint, ['ticket' => $this->getAccessTicket()]);

            if ($result->isOk()) {
                return $result->json;
            }
        }
    }

    /**
     * @param string $type
     * @param string $subtype
     * @return mixed the entry of the subtype if the type and subtype exist, otherwise null
     */
    public function getSubtype($type, $subtype) {
        $endpoint = "/parapheur/types/$type/$subtype";

        $urlParams = [
            'ticket' => $this->getAccessTicket(),
        ];

        $result = $this->get($endpoint, $urlParams);
        if ($result->isOk()) {
            return $result->json;
        }
    }

    /**
     * Adds the office to the subtype
     * @param array $subtype the entry of the subtype
     * @param string $officeId the id of the office
     * @return mixed
     */
    public function addOfficeToSubtype($subtype, $officeId) {
        $endpoint = '/parapheur/types/';
        if ($subtype && $this->getOffice($officeId)) {
            $endpoint .= $subtype['parent'] . '/' . $subtype['id'];
            $subtype['oldId'] = $subtype['id'];
            if (!in_array($officeId, $subtype['parapheurs'])) {
                $subtype['parapheurs'][] = $officeId;
            }

            $result = $this->put($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($subtype));
            if ($result->isOk()) {
                return $result->json;
            }
        }
    }

    /**
     * Removes people in $owners owner from the office $officeId
     * @param string $officeId the id of the office
     * @param array $owners an array of username
     */
    public function removeOwnersFromOffice($officeId, array $owners) {
        $office = $this->getOffice($officeId);
        if ($office) {
            foreach ($owners as $username) {
                if ($this->getUserByUsername($username)
                    && collection($office['proprietaires'])->firstMatch(['username' => $username])
                ) {
                    $office['proprietaires'] = collection($office['proprietaires'])->reject(function ($owner) use ($username) {
                        return $owner['username'] === $username;
                    })->toList();
                }
            }
            $data = ['proprietaires' => $office['proprietaires']];
            $endpoint = '/parapheur/bureaux/' . $officeId;
            $result = $this->put($endpoint, ['ticket' => $this->getAccessTicket()], json_encode($data));

            if ($result->isOk()) {
                return $result->json;
            }
        }
    }


}

