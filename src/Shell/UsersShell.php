<?php

namespace App\Shell;

use Cake\Console\Shell;

/**
 * Users shell command.
 */
class UsersShell extends Shell {

    /**
     * Initializes the Shell
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Users');
    }

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        $parser->setDescription('Users operations.')
            ->addSubcommand('createUser')->setDescription('Creates a new user')
            ->addOptions([
                'name' => ['short' => 'n', 'help' => 'The username of the user', 'default' => ''],
                'password' => ['short' => 'p', 'help' => 'The password of the user', 'default' => ''],
                'mail' => ['short' => 'm', 'help' => 'The mail of the user', 'default' => ''],
                'firstname' => ['short' => 'f', 'help' => 'The firstname of the user', 'default' => ''],
                'lastname' => ['short' => 'l', 'help' => 'The lastname of the user', 'default' => ''],
                'superadmin' => ['short' => 's', 'help' => 'Boolean indicating if the user is a superadmin', 'boolean' => true, 'default' => false],
            ]);
        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main() {
        $this->out($this->OptionParser->help());
    }

    /**
     * Creates a new user
     *
     * @return void
     */
    public function createUser() {
        $user = [
            'name' => $this->param('name'),
            'password' => $this->param('password'),
            'mail' => $this->param('mail'),
            'firstname' => $this->param('firstname'),
            'lastname' => $this->param('lastname'),
            'superadmin' => $this->param('superadmin'),
        ];
        if (empty($this->param('name'))) {
            $user['name'] = $this->in('Username ?');
        }
        if (empty($this->param('password'))) {
            $user['password'] = $this->in('Password ?');
        }
        if (empty($this->param('mail'))) {
            $user['mail'] = $this->in('Email ?');
        }
        if (empty($this->param('firstname'))) {
            $user['firstname'] = $this->in('First name ?');
        }
        if (empty($this->param('lastname'))) {
            $user['lastname'] = $this->in('Last name ?');
        }
        $user['superadmin'] = $this->in('Is superadmin ?', [true, false], false);


        $user = $this->Users->newEntity($user);
        if ($this->Users->save($user)) {
            $this->success('User added:');
            $this->out('Id: ' . $user->id);
            $this->out('Username: ' . $user->name);
            $this->out('Email: ' . $user->mail);
            $this->out('First name: ' . $user->firstname);
            $this->out('Last name: ' . $user->lastname);
            $this->out('Superadmin: ' . $user->superadmin);

        } else {
            $this->err('User could not be added:');
            collection($user->errors())->each(function ($error, $field) {
                $this->out("Field: <error>$field</error> | Error: <error>" . implode(',', $error) . '</error>');
            });
        }
    }
}
