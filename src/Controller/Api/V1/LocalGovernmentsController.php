<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Network\Exception\RoleNotFoundException;
use App\Network\Exception\ServiceNotFoundException;
use App\Network\Exception\UserNotFoundException;
use App\Network\Exception\ValidationException;
use Cake\Network\Exception\NotFoundException;

/**
 * LocalGovernments Controller
 *
 * @property \App\Model\Table\LocalGovernmentsTable $LocalGovernments
 */
class LocalGovernmentsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $this->request->allowMethod('get');

        $localGovernments = $this->paginate($this->LocalGovernments);

        $pagination = $this->request->params['paging']['LocalGovernments'];

        $paginationResponse = [
            'page_count' => $pagination['pageCount'],
            'current_page' => $pagination['page'],
            'has_next_page' => $pagination['nextPage'],
            'has_prev_page' => $pagination['prevPage'],
            'count' => $pagination['count'],
            'limit' => $pagination['limit']
        ];

        $this->set('pagination', $paginationResponse);
        $this->set('localGovernments', $localGovernments);
    }

    /**
     * View method
     *
     * @param string $id Local Government id.
     * @return \Cake\Network\Response|null
     * @throws \App\Network\Exception\LocalGovernmentNotFoundException When record not found.
     */
    public function view($id) {
        $this->request->allowMethod('get');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($id);

        $this->set('localGovernment', $localGovernment);
        $this->set('_serialize', 'localGovernment');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $this->request->allowMethod('post');

        $localGovernment = $this->LocalGovernments->newEntity($this->request->data);

        if ($this->LocalGovernments->save($localGovernment)) {
            if (!isset($localGovernment->pooling_local_governments_id)) {
                $localGovernment->pooling_local_governments_id = null;
            }
            $this->response->statusCode(201);
            $this->set('localGovernment', $localGovernment);
            $this->set('_serialize', 'localGovernment');
        } else {
            $this->response->statusCode(400);
            throw new ValidationException($localGovernment);
        }
    }

    /**
     * Edit method
     *
     * @param string $id Local Government id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id) {

        $this->request->allowMethod('put');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($id);

        $localGovernment = $this->LocalGovernments->patchEntity($localGovernment, $this->request->data);
        if ($this->LocalGovernments->save($localGovernment)) {
            $this->set('localGovernment', $localGovernment);
            $this->set('_serialize', 'localGovernment');
        } else {
            throw new ValidationException($localGovernment);
        }
    }

    /**
     * Delete method
     *
     * @param string $id Local Government id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \App\Network\Exception\LocalGovernmentNotFoundException When record not found.
     */
    public function delete($id) {
        $this->request->allowMethod('delete');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($id);

        if ($this->LocalGovernments->delete($localGovernment)) {
            $this->response->statusCode(204);
        } else {
            throw new ValidationException($localGovernment);
        }
    }

    /**
     * Creates a new user and associate it with the local government
     */
    public function createUser() {
        $this->request->allowMethod('post');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['Users']]);

        $this->request->data['superadmin'] = false;
        $user = $this->LocalGovernments->Users->newEntity($this->request->data);

        if ($this->LocalGovernments->Users->save($user)) {
            $local_government_administrator = isset($this->request->params['local_government_administrator']) ? $this->request->params['local_government_administrator'] : false;
            $user->_joinData = $this->LocalGovernments->UsersMemberships->newEntity(['local_government_administrator' => $local_government_administrator], ['markNew' => true]);


            $this->LocalGovernments->Users->link($localGovernment, [$user]);
//          $localGovernment->users= collection($localGovernment->users)->append([$user])->indexBy('id')->toList();

            $this->response->statusCode(201);

            $this->set('user', $user);
            $this->set('_serialize', 'user');
        } else {
            throw new ValidationException($user);
        }
    }

    /**
     * Get all the users member of the local government
     */
    public function getMembers() {
        $this->request->allowMethod('get');
        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id']);

        $users = $this->LocalGovernments->Users->find()
            ->matching('LocalGovernments', function ($query) use ($localGovernment) {
                return $query->where(['LocalGovernments.id' => $localGovernment->id]);
            });
        $users = $this->paginate($users);

        $pagination = $this->request->params['paging']['Users'];

        $paginationResponse = [
            'page_count' => $pagination['pageCount'],
            'current_page' => $pagination['page'],
            'has_next_page' => $pagination['nextPage'],
            'has_prev_page' => $pagination['prevPage'],
            'count' => $pagination['count'],
            'limit' => $pagination['limit']
        ];

        $this->set('pagination', $paginationResponse);
        $this->set('users', $users);
    }

    /**
     * Add existing users to a local government
     */
    public function addMembers() {
        $this->request->allowMethod('post');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['Users']]);

        $users = [];
        $users_data = $this->request->data['users'];
        $not_existing_users = [];

        foreach ($users_data as $user) {
            try {
                $users[] = $this->LocalGovernments->Users->get($user['user_id']);
            } catch (\Exception $e) {
                $not_existing_users[] = $user['user_id'];
            }
        }

        $number_of_not_existing_users = count($not_existing_users);
        if ($number_of_not_existing_users > 0) {
            $ids = implode($not_existing_users, ',');

            $message = __n(
                "The user with the id $ids does not exist", "The users with the ids $ids do not exist", $number_of_not_existing_users
            );

            throw new UserNotFoundException($message);
        }

        foreach ($users as $user) {
            $index = array_search($user->id, array_column($users_data, 'user_id'));
            $local_government_administrator = isset($users_data[$index]['local_government_administrator']) ? $users_data[$index]['local_government_administrator'] : false;
            $user->_joinData = $this->LocalGovernments->UsersMemberships->newEntity(['local_government_administrator' => $local_government_administrator], ['markNew' => true]);
            $this->LocalGovernments->Users->link($localGovernment, [$user]);
        }

        $localGovernment->users = collection($localGovernment->users)->indexBy('id')->toList();

        collection($localGovernment->users)->each(function ($user) {
            $user->local_government_administrator = $user->_joinData->local_government_administrator;
        });

        $this->set('localGovernment', $localGovernment);
        $this->set('_serialize', 'localGovernment');
    }

    /**
     * Get the user with the id $id if it is member of the local government
     */
    public function viewMember($id) {
        $this->request->allowMethod('get');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['Users']]);

        $member = $localGovernment->getMember($id);

        $this->set('member', $member);
        $this->set('_serialize', 'member');
    }

    /**
     * Edit the member with the id $id if it is member of the local government
     */
    public function editMember($id) {
        $this->request->allowMethod('put');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['Users']]);

        $member = $localGovernment->getMember($id);

        if (isset($this->request->data['superadmin'])) {
            unset($this->request->data['superadmin']);
        }

        if (isset($this->request->data['local_government_administrator'])) {
            $member->_joinData->local_government_administrator = $this->request->data['local_government_administrator'];
        }
        $member = $this->LocalGovernments->Users->patchEntity($member, $this->request->data);

        if ($this->LocalGovernments->Users->save($member)) {
            $this->set('member', $member);
            $this->set('_serialize', 'member');
        } else {
            throw new ValidationException($member);
        }
    }

    /**
     * Removes the user with the id $id of the local government if it is member of
     */
    public function removeMember($id) {
        $this->request->allowMethod('delete');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['Users']]);

        $localGovernment->removeMember($id);

        $this->LocalGovernments->save($localGovernment);
        $this->response->statusCode(204);
    }

    /**
     * Get roles the user $id has access in the local government
     */
    public function viewRolesOfMember($id) {
        $this->request->allowMethod('get');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], [
            'contain' => [
                'UsersMemberships' => [
                    'conditions' => [
                        'UsersMemberships.user_id' => $id
                    ],
                    'Roles'
                ]
            ]
        ]);
        if (empty($localGovernment->users_memberships)) {
            throw new UserNotFoundException("The user with the id $id does not exist");
        }

        $this->set('roles', $localGovernment->users_memberships[0]->roles);
    }

    /**
     * Add roles to a member of the local government
     * Each role MUST be available in the subscription of the local government
     */
    public function addRolesToMember($id) {
        $this->request->allowMethod('post');

        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], [
            'contain' => [
                'Subscriptions' => ['Services' => ['Roles']],
                'UsersMemberships' => [
                    'conditions' => [
                        'UsersMemberships.user_id' => $id
                    ],
                    'Roles'
                ]
            ]
        ]);
        if (empty($localGovernment->users_memberships)) {
            throw new UserNotFoundException("The user with the id $id does not exist");
        }

        $roles = [];
        $roles_ids = $this->request->data['roles'];
        $not_existing_roles = [];

        $rolesLinkedToSubscription = collection($localGovernment->subscription->services)->extract('roles.{*}')->toArray();

        foreach ($roles_ids as $role) {
            $match = collection($rolesLinkedToSubscription)->firstMatch(['id' => $role['role_id']]);
            if ($match) {
                // BelongsToMany::link() does not save if there is the joinData
                unset($match->_joinData);
                $roles[] = $match;
            } else {
                $not_existing_roles[] = $role['role_id'];
            }
        }

        $number_of_not_existing_roles = count($not_existing_roles);
        if ($number_of_not_existing_roles > 0) {
            $ids = implode($not_existing_roles, ',');

            $message = __n(
                "The role with the id $ids does not exist", "The roles with the ids $ids do not exist", $number_of_not_existing_roles
            );
            throw new RoleNotFoundException($message);
        }

        $this->LocalGovernments->UsersMemberships->Roles->link($localGovernment->users_memberships[0], $roles);

        $localGovernment->users_memberships[0]->roles = collection($localGovernment->users_memberships[0]->roles)->indexBy('id')->toList();
        $this->set('roles', $localGovernment->users_memberships[0]->roles);

    }

    /**
     * Get the data of the authenticated user provided by the API of the service $service_id
     *
     * @param $service_id int The id of the service
     */
    public function getServiceData($service_id) {
        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], [
            'contain' => [
//                'Users',
                'Subscriptions' => [
                    'Services' => [
                        'conditions' => [
                            'Services.id' => $service_id
                        ],
                        'Connectors',
                    ]
                ]
            ]]);
        if (empty($localGovernment->subscription->services)) {
            throw new ServiceNotFoundException("The service with the id $service_id does not exist");
        }

        $header = $this->request->getHeader('Authorization');
        if ($header) {
            $arr = explode('Bearer ', $header[0]);
            $header = $arr[1];
        }

        //Initialize phpCAS
        $this->Auth->identify();

        //Get url of the current service
        $url = $localGovernment->subscription->services[0]->connector->url;

        $className = $localGovernment->subscription->services[0]->class;
        if (class_exists($className)) {
            $service = new $className($localGovernment->name, $url, $header);

            $this->set('response', $service->getData());
            $this->set('_serialize', 'response');
        } else {
            //FIXME throw an exception for the moment
            throw new NotFoundException("The class $className does not exist");
        }
    }

    /**
     * Get the data of the authenticated user from all the services he has access to
     */
    public function getServicesData() {
        $user = $this->Auth->identify();
        $endData = [];
        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], [
            'contain' => [
                'Subscriptions' => [
                    'Services' => [
                        'Connectors',
                    ]
                ],
                'UsersMemberships' => [
                    'conditions' => [
                        'UsersMemberships.user_id' => $user['id']
                    ],
                    'Roles',
                ]
            ]]);
        $header = $this->request->getHeader('Authorization');
        if ($header) {
            $arr = explode('Bearer ', $header[0]);
            $header = $arr[1];
        }
        $serviceIds = collection($localGovernment->users_memberships[0]->roles)->extract('service_id')->toArray();

        $servicesToQuery = collection($localGovernment->subscription->services)
            ->filter(function ($service) use ($serviceIds) {
                return in_array($service['id'], $serviceIds, true);
            });

        foreach ($servicesToQuery as $serviceToQuery) {
            $url = $serviceToQuery->connector->url;
            $serviceData =  [
                'name' => $serviceToQuery['label'],
                'url' => $url
            ];

            $className = $serviceToQuery->class;
            if (class_exists($className)) {
                $service = new $className($localGovernment->name, $url, $header);
                $serviceData['data'] = $service->getData();
                $endData[] = $serviceData;
            }
        }
        $this->set('services', $endData);
    }

    /**
     * Get roles available to set to members of the local government
     */
    public function getRoles() {
        $localGovernment = $this->LocalGovernments->getLocalGovernment($this->request->getParam('local_government_id'), [
            'contain' => [
                'Subscriptions' => [
                    'Services' => [
                        'Roles'
                    ]
                ]
            ]]);
        $this->set('roles', collection($localGovernment->subscription->services)->extract('roles.{*}')->toArray());
    }

    public function isAuthorized($user) {
        $localGovernmentAdministratorAuthorizedActions = ['createUser', 'getMembers', 'addMembers', 'viewMember', 'editMember', 'removeMember', 'viewRolesOfMember', 'addRolesToMember', 'removeRoleToMember', 'getRoles'];

        if ($this->request->action === 'getServiceData') {
            $local_government = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['UsersMemberships']]);
            return $local_government->isMember($user['id']);
        } elseif ($this->request->action === 'view') {
            $local_government = $this->LocalGovernments->getLocalGovernment($this->request->params['id'], ['contain' => ['UsersMemberships', 'PoolingLocalGovernments' => ['UsersMemberships']]]);
            if ($local_government->isMember($user['id'])) {
                return true;
            } elseif ($local_government->pooling_local_government && $local_government->pooling_local_government->isAdministrator($user['id'])) {
                return true;
            }
        } elseif (in_array($this->request->action, ['edit', 'delete'])) {
            $local_government = $this->LocalGovernments->getLocalGovernment($this->request->params['id'], ['contain' => ['PoolingLocalGovernments' => ['UsersMemberships']]]);
            if ($local_government->pooling_local_government && $local_government->pooling_local_government->isAdministrator($user['id'])) {
                return true;
            }
        } elseif (in_array($this->request->action, $localGovernmentAdministratorAuthorizedActions)) {
            $local_government = $this->LocalGovernments->getLocalGovernment($this->request->params['local_government_id'], ['contain' => ['UsersMemberships', 'PoolingLocalGovernments' => ['UsersMemberships']]]);
            if ($local_government->isAdministrator($user['id'])) {
                return true;
            } elseif ($local_government->pooling_local_government && $local_government->pooling_local_government->isAdministrator($user['id'])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }


}
