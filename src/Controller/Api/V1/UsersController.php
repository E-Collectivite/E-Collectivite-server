<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Network\Exception\UserNotFoundException;
use App\Network\Exception\ValidationException;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    public $paginate = [
        'page' => 1,
        'limit' => 20,
        'maxLimit' => 100,
        'fields' => [
            'id', 'name', 'mail', 'firstname', 'lastname', 'superadmin'
        ],
        'sortWhitelist' => [
            'id', 'name', 'mail', 'firstname', 'lastname', 'superadmin'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->request->allowMethod('get');

        $users = $this->paginate($this->Users);
        $pagination = $this->request->params['paging']['Users'];

        $paginationResponse = [
            'page_count' => $pagination['pageCount'],
            'current_page' => $pagination['page'],
            'has_next_page' => $pagination['nextPage'],
            'has_prev_page' => $pagination['prevPage'],
            'count' => $pagination['count'],
            'limit' => $pagination['limit']
        ];

        $this->set('pagination', $paginationResponse);
        $this->set('users', $users);
    }

    /**
     * View method
     *
     * @param string $id User id.
     * @return \Cake\Network\Response|null
     * @throws UserNotFoundException When record not found.
     */
    public function view($id) {
        $this->request->allowMethod('get');

        $user = $this->Users->getUser($id);

        $this->set('user', $user);
        $this->set('_serialize', 'user');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->request->allowMethod('post');

        if (!isset($this->request->data['superadmin'])) {
            $this->request->data['superadmin'] = false;
        }

        $user = $this->Users->newEntity($this->request->data);
        if ($this->Users->save($user)) {
            $this->response->statusCode(201);
            $this->set('user', $user);
            $this->set('_serialize', 'user');
        } else {
            $this->response->statusCode(400);
            throw new ValidationException($user);
        }
    }

    /**
     * Edit method
     *
     * @param string $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws UserNotFoundException When record not found.
     */
    public function edit($id) {
        $this->request->allowMethod('put');

        $oldUser = $this->Users->getUser($id);

        if (isset($this->request->data['name'])) {
            unset($this->request->data['name']);
        }

        $newUser = $this->Users->patchEntity($oldUser, $this->request->data);

        if ($this->Users->save($newUser)) {
            $this->response->statusCode(200);
            $this->set('user', $newUser);
            $this->set('_serialize', 'user');
        } else {
            $this->response->statusCode(400);
            throw new ValidationException($newUser);
        }
    }

    /**
     * Delete method
     *
     * @param string $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws UserNotFoundException When record not found.
     */
    public function delete($id) {
        $this->request->allowMethod('delete');

        $user = $this->Users->getUser($id);

        if ($this->Users->delete($user)) {
            $this->response->statusCode(204);
        } else {
            $this->response->statusCode(400);
            throw new ValidationException($user);
        }
    }

    /**
     * Issues a new JWT
     *
     * @throws \Cake\Network\Exception\UnauthorizedException When provided credentials are incorrect
     */
    public function token() {
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException('Invalid username or password');
        }
        if (isset($user['password'])) {
            unset($user['password']);
        }

        $issuedAt = Time::now();
        $token = [
            'iss' => Configure::read('App.fullBaseUrl'),
            'iat' => $issuedAt->getTimestamp(),
            'exp' => $issuedAt->addDay()->getTimestamp(),
            'id' => $user['id'],
            'sub' => $user['name'],
        ];
        $jwt = JWT::encode($token, Security::salt(), 'HS256');
        $this->set('token', $jwt);
        $this->set('user', $user);
        $this->set('_serialize', true);
    }


    public function login() {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl($this->Auth->getConfig('loginRedirect'). $this->request->getQuery('redirect_url')));
            }
            $this->Flash->error(__('Username or password is incorrect'));
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['logout', 'token']);
    }

}
