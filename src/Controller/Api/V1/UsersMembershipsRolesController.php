<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * UsersMembershipsRoles Controller
 *
 * @property \App\Model\Table\UsersMembershipsRolesTable $UsersMembershipsRoles
 */
class UsersMembershipsRolesController extends AppController {

    public function deleteRole($role_id) {
        $this->request->allowMethod('delete');

        $user_id = $this->request->params['user_id'];
        $localGovernment_id = $this->request->params['local_government_id'];

        $userMembershipRole = $this->UsersMembershipsRoles->find('userRole', [
            'role_id' => $role_id,
            'local_government_id' => $localGovernment_id,
            'user_id' => $user_id
        ])->first();

        $this->UsersMembershipsRoles->delete($userMembershipRole);
        $this->response->statusCode(204);
    }
}
