<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Network\Exception\ServiceNotFoundException;
use App\Network\Exception\ValidationException;

/**
 * Subscriptions Controller
 *
 * @property \App\Model\Table\SubscriptionsTable $Subscriptions
 */
class SubscriptionsController extends AppController {

    public $paginate = [
      'page' => 1,
      'limit' => 20,
      'maxLimit' => 100,
      'fields' => [
        'id', 'name'
      ],
      'sortWhitelist' => [
        'id', 'name'
      ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->request->allowMethod('get');

        $subscriptions = $this->paginate($this->Subscriptions);

        $pagination = $this->request->params['paging']['Subscriptions'];

        $paginationResponse = [
          'page_count' => $pagination['pageCount'],
          'current_page' => $pagination['page'],
          'has_next_page' => $pagination['nextPage'],
          'has_prev_page' => $pagination['prevPage'],
          'count' => $pagination['count'],
          'limit' => $pagination['limit']
        ];

        $this->set('pagination', $paginationResponse);
        $this->set('subscriptions', $subscriptions);
    }

    /**
     * View method
     *
     * @param string $id Subscription id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id) {
        $this->request->allowMethod('get');

        $subscription = $this->Subscriptions->getSubscription($id, ['contain' => ['Services']]);

        $this->set('subscription', $subscription);
        $this->set('_serialize', 'subscription');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->request->allowMethod('post');

        $subscription = $this->Subscriptions->newEntity($this->request->data);
        if ($this->Subscriptions->save($subscription)) {
            $this->response->statusCode(201);
            $this->set('subscription', $subscription);
            $this->set('_serialize', 'subscription');
        }
        else {
            $this->response->statusCode(400);
            throw new ValidationException($subscription);
        }
    }

    /**
     * Edit method
     *
     * @param string $id Subscription id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id) {
        $this->request->allowMethod('put');

        $subscription = $this->Subscriptions->getSubscription($id);

        $subscription = $this->Subscriptions->patchEntity($subscription, $this->request->data);
        if ($this->Subscriptions->save($subscription)) {
            $this->set('subscription', $subscription);
            $this->set('_serialize', 'subscription');
        }
        else {
            throw new ValidationException($subscription);
        }
    }

    /**
     * Delete method
     *
     * @param string $id Subscription id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id) {
        $this->request->allowMethod('delete');

        $subscription = $this->Subscriptions->getSubscription($id);

        $this->Subscriptions->delete($subscription);
        $this->response->statusCode(204);
    }

    public function addServices() {
        $this->request->allowMethod('post');

        $subscription = $this->Subscriptions->getSubscription($this->request->params['subscription_id'], ['contain' => 'Services']);

        $services = [];
        $services_ids = $this->request->data['services'];
        $not_existing_services = [];

        if ($services_ids) {
            foreach ($services_ids as $service) {
                try {
                    $services[] = $this->Subscriptions->Services->get($service['service_id']);
                }
                catch (\Exception $e) {
                    $not_existing_services[] = $service['service_id'];
                }
            }
            $number_of_not_existing_services = count($not_existing_services);
            if ($number_of_not_existing_services > 0) {
                $ids = implode($not_existing_services, ',');

                $message = __n(
                    "The service with the id $ids does not exist", "The services with the ids $ids do not exist", $number_of_not_existing_services
                );
                throw new ServiceNotFoundException($message);
            }

            foreach ($services as $service) {
                $this->Subscriptions->Services->link($subscription, [$service]);
            }

            $subscription->cleanup();
        }


        $this->set('subscription', $subscription);
        $this->set('_serialize', 'subscription');
    }

    public function removeService($service_id) {
        $this->request->allowMethod('delete');

        $subscription = $this->Subscriptions->getSubscription($this->request->params['subscription_id'], ['contain' => 'Services']);

        $serviceExists = $subscription->removeService($service_id);

        if (!$serviceExists) {
            throw new ServiceNotFoundException("The service with the id $service_id does not exist");
        }

        $this->Subscriptions->save($subscription);
    }

}
