<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Network\Exception\RoleNotFoundException;
use App\Network\Exception\ValidationException;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 */
class RolesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $connector = $this->Roles->Services->Connectors->getConnector($this->request->params['connector_id']);
        $service = $this->Roles->Services->getService($this->request->params['service_id'], [
            'conditions' => ['Services.connector_id' => $connector->id],
            'contain' => ['Roles']
        ]);

        $this->set('roles', $service->roles);
        $this->set('_serialize', 'roles');
    }

    /**
     * View method
     *
     * @param string $id Role id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id) {
        $connector = $this->Roles->Services->Connectors->getConnector($this->request->params['connector_id']);
        $service = $this->Roles->Services->getService($this->request->params['service_id'], [
            'conditions' => ['Services.connector_id' => $connector->id],
            'contain' => ['Roles' => [
                'conditions' => [
                    'Roles.id' => $id
                ],
            ]
            ]
        ]);
        if (empty($service->roles)) {
            throw new RoleNotFoundException("The role with the id $id does not exist");
        }

        $this->set('role', $service->roles[0]);
        $this->set('_serialize', 'role');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->request->allowMethod('post');
        $connector = $this->Roles->Services->Connectors->getConnector($this->request->params['connector_id']);
        $service = $this->Roles->Services->getService($this->request->params['service_id'], [
            'conditions' => ['Services.connector_id' => $connector->id]]);

        $this->request->data['service_id'] = $service->id;

        $role = $this->Roles->newEntity($this->request->data);
        if ($this->Roles->save($role)) {
            $this->response->statusCode(201);
            $this->set('role', $role);
            $this->set('_serialize', 'role');
        } else {
            $this->response->statusCode(400);
            throw new ValidationException($role);
        }
    }

    /**
     * Edit method
     *
     * @param string $id Role id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id) {
        $this->request->allowMethod('put');

        $connector = $this->Roles->Services->Connectors->getConnector($this->request->params['connector_id']);
        $service = $this->Roles->Services->getService($this->request->params['service_id'], [
            'conditions' => ['Services.connector_id' => $connector->id],
            'contain' => ['Roles' => [
                'conditions' => [
                    'Roles.id' => $id
                ],
            ]
            ]
        ]);
        if (empty($service->roles)) {
            throw new RoleNotFoundException("The role with the id $id does not exist");
        }

        $role = $this->Roles->patchEntity($service->roles[0], $this->request->data);

        if ($this->Roles->save($role)) {
            $this->set('role', $role);
            $this->set('_serialize', 'role');
        } else {
            throw new ValidationException($service);
        }

    }

    /**
     * Delete method
     *
     * @param string $id Role id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id) {
        $this->request->allowMethod('delete');

        $connector = $this->Roles->Services->Connectors->getConnector($this->request->params['connector_id']);

        $service = $this->Roles->Services->getService($this->request->params['service_id'], [
            'conditions' => ['Services.connector_id' => $connector->id],
            'contain' => ['Roles' => [
                'conditions' => [
                    'Roles.id' => $id
                ],
            ]
            ]
        ]);
        if (empty($service->roles)) {
            throw new RoleNotFoundException("The role with the id $id does not exist");
        }

        $this->Roles->delete($service->roles[0]);
        $this->response->statusCode(204);
    }
}
