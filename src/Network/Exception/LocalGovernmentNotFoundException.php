<?php

namespace App\Network\Exception;

use Cake\Network\Exception\NotFoundException;

class LocalGovernmentNotFoundException extends NotFoundException {
  
}
