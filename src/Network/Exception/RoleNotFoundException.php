<?php

namespace App\Network\Exception;

use Cake\Network\Exception\NotFoundException;

class RoleNotFoundException extends NotFoundException {

}
